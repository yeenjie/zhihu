package com.phoenix.zhihu.entity;

public class QuestionList {
    private String questionname;
    private String questiondescribe;
    private int id;  //table answer
    private String content;

    public String getQuestionname() {
        return questionname;
    }

    public void setQuestionname(String questionname) {
        this.questionname = questionname;
    }

    public String getQuestiondescribe() {
        return questiondescribe;
    }

    public void setQuestiondescribe(String questiondescribe) {
        this.questiondescribe = questiondescribe;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
