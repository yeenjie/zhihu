package com.phoenix.zhihu.entity.resultObject;

public class User_Admin {

    private long id;
    private String username;
    private long telnum;
    private String email;
    private boolean isShow1;
    private boolean isShow2;

    private int role;

    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public long getTelnum() {
        return telnum;
    }

    public void setTelnum(long telnum) {
        this.telnum = telnum;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isShow1() {
        return isShow1;
    }

    public void setShow1(boolean show1) {
        isShow1 = show1;
    }

    public boolean isShow2() {
        return isShow2;
    }

    public void setShow2(boolean show2) {
        isShow2 = show2;
    }
}
