package com.phoenix.zhihu.entity.resultObject;

import java.util.Date;

public class Message_User {
    private Integer id;

    private Integer receiverid;

    private Integer senderid;

    private String sendername;

    private String content;

    private String receivername;
    private String dp;

    public String getDp() {
        return dp;
    }

    public void setDp(String dp) {
        this.dp = dp;
    }

    public String getReceivername() {
        return receivername;
    }

    public void setReceivername(String receivername) {
        this.receivername = receivername;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getReceiverid() {
        return receiverid;
    }

    public void setReceiverid(Integer receiverid) {
        this.receiverid = receiverid;
    }

    public Integer getSenderid() {
        return senderid;
    }

    public void setSenderid(Integer senderid) {
        this.senderid = senderid;
    }

    public String getSendername() {
        return sendername;
    }

    public void setSendername(String sendername) {
        this.sendername = sendername;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getCtreatetime() {
        return ctreatetime;
    }

    public void setCtreatetime(Date ctreatetime) {
        this.ctreatetime = ctreatetime;
    }

    private Date ctreatetime;
}
