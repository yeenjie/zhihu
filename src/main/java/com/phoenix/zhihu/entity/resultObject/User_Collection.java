package com.phoenix.zhihu.entity.resultObject;

import java.util.Date;

public class User_Collection {
    private long userid;
    private String content;
    private long questionid;
    private String questionname;
    private Date answertime;

    public String getDp() {
        return dp;
    }

    public void setDp(String dp) {
        this.dp = dp;
    }

    private String dp;
    public long getAnswerid() {
        return answerid;
    }

    public void setAnswerid(long answerid) {
        this.answerid = answerid;
    }

    private long answerid;
    private String show1;
    private String show2;
    private String show3;
    private long approve;
    private long commenttimes;
    public long getApprove() {
        return approve;
    }

    public void setApprove(long approve) {
        this.approve = approve;
    }

    public long getCommenttimes() {
        return commenttimes;
    }

    public void setCommenttimes(long commenttimes) {
        this.commenttimes = commenttimes;
    }


    public String getShow1() {
        return show1;
    }

    public void setShow1(String show1) {
        this.show1 = show1;
    }

    public String getShow2() {
        return show2;
    }

    public void setShow2(String show2) {
        this.show2 = show2;
    }

    public String getShow3() {
        return show3;
    }

    public void setShow3(String show3) {
        this.show3 = show3;
    }

    public String getShow4() {
        return show4;
    }

    public void setShow4(String show4) {
        this.show4 = show4;
    }

    private String show4;


    public long getUserid() {

        return userid;
    }

    public void setUserid(long userid) {
        this.userid = userid;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public long getQuestionid() {
        return questionid;
    }

    public void setQuestionid(long questionid) {
        this.questionid = questionid;
    }

    public String getQuestionname() {
        return questionname;
    }

    public void setQuestionname(String questionname) {
        this.questionname = questionname;
    }

    public Date getAnswertime() {
        return answertime;
    }

    public void setAnswertime(Date answertime) {
        this.answertime = answertime;
    }

}
