package com.phoenix.zhihu.entity.resultObject;

import java.util.Date;

public class Question_Admin {
    private long id;
    private String questionname;
    private long userid;
    private String username;
    private Date createtime;
    private long skimtimes;

    public long getId() {

        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getQuestionname() {
        return questionname;
    }

    public void setQuestionname(String questionname) {
        this.questionname = questionname;
    }

    public long getUserid() {
        return userid;
    }

    public void setUserid(long userid) {
        this.userid = userid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    public long getSkimtimes() {
        return skimtimes;
    }

    public void setSkimtimes(long skimtimes) {
        this.skimtimes = skimtimes;
    }
}
