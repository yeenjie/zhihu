package com.phoenix.zhihu.entity;

import java.util.Date;

public class Topiccategory {
    private Integer id;

    private String categoryname;

    private String resume;

    private String image;

    private Date ctreatetime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCategoryname() {
        return categoryname;
    }

    public void setCategoryname(String categoryname) {
        this.categoryname = categoryname == null ? null : categoryname.trim();
    }

    public String getResume() {
        return resume;
    }

    public void setResume(String resume) {
        this.resume = resume == null ? null : resume.trim();
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image == null ? null : image.trim();
    }

    public Date getCtreatetime() {
        return ctreatetime;
    }

    public void setCtreatetime(Date ctreatetime) {
        this.ctreatetime = ctreatetime;
    }
}