package com.phoenix.zhihu.entity;

import java.util.Date;

public class Topic {
    private Integer id;

    private Integer categoryid;

    private String topicname;

    private String topicdescribe;

    private String image;

    private Date ctreatetime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCategoryid() {
        return categoryid;
    }

    public void setCategoryid(Integer categoryid) {
        this.categoryid = categoryid;
    }

    public String getTopicname() {
        return topicname;
    }

    public void setTopicname(String topicname) {
        this.topicname = topicname == null ? null : topicname.trim();
    }

    public String getTopicdescribe() {
        return topicdescribe;
    }

    public void setTopicdescribe(String topicdescribe) {
        this.topicdescribe = topicdescribe == null ? null : topicdescribe.trim();
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image == null ? null : image.trim();
    }

    public Date getCtreatetime() {
        return ctreatetime;
    }

    public void setCtreatetime(Date ctreatetime) {
        this.ctreatetime = ctreatetime;
    }
}