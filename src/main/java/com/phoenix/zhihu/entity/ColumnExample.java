package com.phoenix.zhihu.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ColumnExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public ColumnExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andColumnnameIsNull() {
            addCriterion("columnname is null");
            return (Criteria) this;
        }

        public Criteria andColumnnameIsNotNull() {
            addCriterion("columnname is not null");
            return (Criteria) this;
        }

        public Criteria andColumnnameEqualTo(String value) {
            addCriterion("columnname =", value, "columnname");
            return (Criteria) this;
        }

        public Criteria andColumnnameNotEqualTo(String value) {
            addCriterion("columnname <>", value, "columnname");
            return (Criteria) this;
        }

        public Criteria andColumnnameGreaterThan(String value) {
            addCriterion("columnname >", value, "columnname");
            return (Criteria) this;
        }

        public Criteria andColumnnameGreaterThanOrEqualTo(String value) {
            addCriterion("columnname >=", value, "columnname");
            return (Criteria) this;
        }

        public Criteria andColumnnameLessThan(String value) {
            addCriterion("columnname <", value, "columnname");
            return (Criteria) this;
        }

        public Criteria andColumnnameLessThanOrEqualTo(String value) {
            addCriterion("columnname <=", value, "columnname");
            return (Criteria) this;
        }

        public Criteria andColumnnameLike(String value) {
            addCriterion("columnname like", value, "columnname");
            return (Criteria) this;
        }

        public Criteria andColumnnameNotLike(String value) {
            addCriterion("columnname not like", value, "columnname");
            return (Criteria) this;
        }

        public Criteria andColumnnameIn(List<String> values) {
            addCriterion("columnname in", values, "columnname");
            return (Criteria) this;
        }

        public Criteria andColumnnameNotIn(List<String> values) {
            addCriterion("columnname not in", values, "columnname");
            return (Criteria) this;
        }

        public Criteria andColumnnameBetween(String value1, String value2) {
            addCriterion("columnname between", value1, value2, "columnname");
            return (Criteria) this;
        }

        public Criteria andColumnnameNotBetween(String value1, String value2) {
            addCriterion("columnname not between", value1, value2, "columnname");
            return (Criteria) this;
        }

        public Criteria andAuthoridIsNull() {
            addCriterion("authorid is null");
            return (Criteria) this;
        }

        public Criteria andAuthoridIsNotNull() {
            addCriterion("authorid is not null");
            return (Criteria) this;
        }

        public Criteria andAuthoridEqualTo(Integer value) {
            addCriterion("authorid =", value, "authorid");
            return (Criteria) this;
        }

        public Criteria andAuthoridNotEqualTo(Integer value) {
            addCriterion("authorid <>", value, "authorid");
            return (Criteria) this;
        }

        public Criteria andAuthoridGreaterThan(Integer value) {
            addCriterion("authorid >", value, "authorid");
            return (Criteria) this;
        }

        public Criteria andAuthoridGreaterThanOrEqualTo(Integer value) {
            addCriterion("authorid >=", value, "authorid");
            return (Criteria) this;
        }

        public Criteria andAuthoridLessThan(Integer value) {
            addCriterion("authorid <", value, "authorid");
            return (Criteria) this;
        }

        public Criteria andAuthoridLessThanOrEqualTo(Integer value) {
            addCriterion("authorid <=", value, "authorid");
            return (Criteria) this;
        }

        public Criteria andAuthoridIn(List<Integer> values) {
            addCriterion("authorid in", values, "authorid");
            return (Criteria) this;
        }

        public Criteria andAuthoridNotIn(List<Integer> values) {
            addCriterion("authorid not in", values, "authorid");
            return (Criteria) this;
        }

        public Criteria andAuthoridBetween(Integer value1, Integer value2) {
            addCriterion("authorid between", value1, value2, "authorid");
            return (Criteria) this;
        }

        public Criteria andAuthoridNotBetween(Integer value1, Integer value2) {
            addCriterion("authorid not between", value1, value2, "authorid");
            return (Criteria) this;
        }

        public Criteria andCreatetimeIsNull() {
            addCriterion("createtime is null");
            return (Criteria) this;
        }

        public Criteria andCreatetimeIsNotNull() {
            addCriterion("createtime is not null");
            return (Criteria) this;
        }

        public Criteria andCreatetimeEqualTo(Date value) {
            addCriterion("createtime =", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeNotEqualTo(Date value) {
            addCriterion("createtime <>", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeGreaterThan(Date value) {
            addCriterion("createtime >", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeGreaterThanOrEqualTo(Date value) {
            addCriterion("createtime >=", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeLessThan(Date value) {
            addCriterion("createtime <", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeLessThanOrEqualTo(Date value) {
            addCriterion("createtime <=", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeIn(List<Date> values) {
            addCriterion("createtime in", values, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeNotIn(List<Date> values) {
            addCriterion("createtime not in", values, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeBetween(Date value1, Date value2) {
            addCriterion("createtime between", value1, value2, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeNotBetween(Date value1, Date value2) {
            addCriterion("createtime not between", value1, value2, "createtime");
            return (Criteria) this;
        }

        public Criteria andColumndescribeIsNull() {
            addCriterion("columndescribe is null");
            return (Criteria) this;
        }

        public Criteria andColumndescribeIsNotNull() {
            addCriterion("columndescribe is not null");
            return (Criteria) this;
        }

        public Criteria andColumndescribeEqualTo(String value) {
            addCriterion("columndescribe =", value, "columndescribe");
            return (Criteria) this;
        }

        public Criteria andColumndescribeNotEqualTo(String value) {
            addCriterion("columndescribe <>", value, "columndescribe");
            return (Criteria) this;
        }

        public Criteria andColumndescribeGreaterThan(String value) {
            addCriterion("columndescribe >", value, "columndescribe");
            return (Criteria) this;
        }

        public Criteria andColumndescribeGreaterThanOrEqualTo(String value) {
            addCriterion("columndescribe >=", value, "columndescribe");
            return (Criteria) this;
        }

        public Criteria andColumndescribeLessThan(String value) {
            addCriterion("columndescribe <", value, "columndescribe");
            return (Criteria) this;
        }

        public Criteria andColumndescribeLessThanOrEqualTo(String value) {
            addCriterion("columndescribe <=", value, "columndescribe");
            return (Criteria) this;
        }

        public Criteria andColumndescribeLike(String value) {
            addCriterion("columndescribe like", value, "columndescribe");
            return (Criteria) this;
        }

        public Criteria andColumndescribeNotLike(String value) {
            addCriterion("columndescribe not like", value, "columndescribe");
            return (Criteria) this;
        }

        public Criteria andColumndescribeIn(List<String> values) {
            addCriterion("columndescribe in", values, "columndescribe");
            return (Criteria) this;
        }

        public Criteria andColumndescribeNotIn(List<String> values) {
            addCriterion("columndescribe not in", values, "columndescribe");
            return (Criteria) this;
        }

        public Criteria andColumndescribeBetween(String value1, String value2) {
            addCriterion("columndescribe between", value1, value2, "columndescribe");
            return (Criteria) this;
        }

        public Criteria andColumndescribeNotBetween(String value1, String value2) {
            addCriterion("columndescribe not between", value1, value2, "columndescribe");
            return (Criteria) this;
        }

        public Criteria andReportnumberIsNull() {
            addCriterion("reportnumber is null");
            return (Criteria) this;
        }

        public Criteria andReportnumberIsNotNull() {
            addCriterion("reportnumber is not null");
            return (Criteria) this;
        }

        public Criteria andReportnumberEqualTo(Integer value) {
            addCriterion("reportnumber =", value, "reportnumber");
            return (Criteria) this;
        }

        public Criteria andReportnumberNotEqualTo(Integer value) {
            addCriterion("reportnumber <>", value, "reportnumber");
            return (Criteria) this;
        }

        public Criteria andReportnumberGreaterThan(Integer value) {
            addCriterion("reportnumber >", value, "reportnumber");
            return (Criteria) this;
        }

        public Criteria andReportnumberGreaterThanOrEqualTo(Integer value) {
            addCriterion("reportnumber >=", value, "reportnumber");
            return (Criteria) this;
        }

        public Criteria andReportnumberLessThan(Integer value) {
            addCriterion("reportnumber <", value, "reportnumber");
            return (Criteria) this;
        }

        public Criteria andReportnumberLessThanOrEqualTo(Integer value) {
            addCriterion("reportnumber <=", value, "reportnumber");
            return (Criteria) this;
        }

        public Criteria andReportnumberIn(List<Integer> values) {
            addCriterion("reportnumber in", values, "reportnumber");
            return (Criteria) this;
        }

        public Criteria andReportnumberNotIn(List<Integer> values) {
            addCriterion("reportnumber not in", values, "reportnumber");
            return (Criteria) this;
        }

        public Criteria andReportnumberBetween(Integer value1, Integer value2) {
            addCriterion("reportnumber between", value1, value2, "reportnumber");
            return (Criteria) this;
        }

        public Criteria andReportnumberNotBetween(Integer value1, Integer value2) {
            addCriterion("reportnumber not between", value1, value2, "reportnumber");
            return (Criteria) this;
        }

        public Criteria andAttentionnumberIsNull() {
            addCriterion("attentionnumber is null");
            return (Criteria) this;
        }

        public Criteria andAttentionnumberIsNotNull() {
            addCriterion("attentionnumber is not null");
            return (Criteria) this;
        }

        public Criteria andAttentionnumberEqualTo(Integer value) {
            addCriterion("attentionnumber =", value, "attentionnumber");
            return (Criteria) this;
        }

        public Criteria andAttentionnumberNotEqualTo(Integer value) {
            addCriterion("attentionnumber <>", value, "attentionnumber");
            return (Criteria) this;
        }

        public Criteria andAttentionnumberGreaterThan(Integer value) {
            addCriterion("attentionnumber >", value, "attentionnumber");
            return (Criteria) this;
        }

        public Criteria andAttentionnumberGreaterThanOrEqualTo(Integer value) {
            addCriterion("attentionnumber >=", value, "attentionnumber");
            return (Criteria) this;
        }

        public Criteria andAttentionnumberLessThan(Integer value) {
            addCriterion("attentionnumber <", value, "attentionnumber");
            return (Criteria) this;
        }

        public Criteria andAttentionnumberLessThanOrEqualTo(Integer value) {
            addCriterion("attentionnumber <=", value, "attentionnumber");
            return (Criteria) this;
        }

        public Criteria andAttentionnumberIn(List<Integer> values) {
            addCriterion("attentionnumber in", values, "attentionnumber");
            return (Criteria) this;
        }

        public Criteria andAttentionnumberNotIn(List<Integer> values) {
            addCriterion("attentionnumber not in", values, "attentionnumber");
            return (Criteria) this;
        }

        public Criteria andAttentionnumberBetween(Integer value1, Integer value2) {
            addCriterion("attentionnumber between", value1, value2, "attentionnumber");
            return (Criteria) this;
        }

        public Criteria andAttentionnumberNotBetween(Integer value1, Integer value2) {
            addCriterion("attentionnumber not between", value1, value2, "attentionnumber");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}