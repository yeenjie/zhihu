package com.phoenix.zhihu.entity;

import java.util.Date;

public class Message {
    private Integer id;

    private Integer receiverid;

    private Integer senderid;

    private String content;

    private Date ctreatetime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getReceiverid() {
        return receiverid;
    }

    public void setReceiverid(Integer receiverid) {
        this.receiverid = receiverid;
    }

    public Integer getSenderid() {
        return senderid;
    }

    public void setSenderid(Integer senderid) {
        this.senderid = senderid;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }

    public Date getCtreatetime() {
        return ctreatetime;
    }

    public void setCtreatetime(Date ctreatetime) {
        this.ctreatetime = ctreatetime;
    }
}