package com.phoenix.zhihu.entity;

public class Follow {

    private long id;
    private long uid;
    private long fid;

    public long getId() {

        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getUid() {
        return uid;
    }

    public void setUid(long uid) {
        this.uid = uid;
    }

    public long getFid() {
        return fid;
    }

    public void setFid(long fid) {
        this.fid = fid;
    }
}
