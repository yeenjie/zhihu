package com.phoenix.zhihu.entity;

import java.util.Date;

public class Arctic {
    private Integer id;

    private String articlename;

    private Integer authorid;

    private Date createtime;

    private Integer agree;

    private Integer commentnumber;

    private String articlepicture;

    private Integer columnid;

    private String articletext;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getArticlename() {
        return articlename;
    }

    public void setArticlename(String articlename) {
        this.articlename = articlename == null ? null : articlename.trim();
    }

    public Integer getAuthorid() {
        return authorid;
    }

    public void setAuthorid(Integer authorid) {
        this.authorid = authorid;
    }

    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    public Integer getAgree() {
        return agree;
    }

    public void setAgree(Integer agree) {
        this.agree = agree;
    }

    public Integer getCommentnumber() {
        return commentnumber;
    }

    public void setCommentnumber(Integer commentnumber) {
        this.commentnumber = commentnumber;
    }

    public String getArticlepicture() {
        return articlepicture;
    }

    public void setArticlepicture(String articlepicture) {
        this.articlepicture = articlepicture == null ? null : articlepicture.trim();
    }

    public Integer getColumnid() {
        return columnid;
    }

    public void setColumnid(Integer columnid) {
        this.columnid = columnid;
    }

    public String getArticletext() {
        return articletext;
    }

    public void setArticletext(String articletext) {
        this.articletext = articletext == null ? null : articletext.trim();
    }
}