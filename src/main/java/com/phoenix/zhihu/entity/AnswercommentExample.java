package com.phoenix.zhihu.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class AnswercommentExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public AnswercommentExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andCriticsidIsNull() {
            addCriterion("criticsid is null");
            return (Criteria) this;
        }

        public Criteria andCriticsidIsNotNull() {
            addCriterion("criticsid is not null");
            return (Criteria) this;
        }

        public Criteria andCriticsidEqualTo(Integer value) {
            addCriterion("criticsid =", value, "criticsid");
            return (Criteria) this;
        }

        public Criteria andCriticsidNotEqualTo(Integer value) {
            addCriterion("criticsid <>", value, "criticsid");
            return (Criteria) this;
        }

        public Criteria andCriticsidGreaterThan(Integer value) {
            addCriterion("criticsid >", value, "criticsid");
            return (Criteria) this;
        }

        public Criteria andCriticsidGreaterThanOrEqualTo(Integer value) {
            addCriterion("criticsid >=", value, "criticsid");
            return (Criteria) this;
        }

        public Criteria andCriticsidLessThan(Integer value) {
            addCriterion("criticsid <", value, "criticsid");
            return (Criteria) this;
        }

        public Criteria andCriticsidLessThanOrEqualTo(Integer value) {
            addCriterion("criticsid <=", value, "criticsid");
            return (Criteria) this;
        }

        public Criteria andCriticsidIn(List<Integer> values) {
            addCriterion("criticsid in", values, "criticsid");
            return (Criteria) this;
        }

        public Criteria andCriticsidNotIn(List<Integer> values) {
            addCriterion("criticsid not in", values, "criticsid");
            return (Criteria) this;
        }

        public Criteria andCriticsidBetween(Integer value1, Integer value2) {
            addCriterion("criticsid between", value1, value2, "criticsid");
            return (Criteria) this;
        }

        public Criteria andCriticsidNotBetween(Integer value1, Integer value2) {
            addCriterion("criticsid not between", value1, value2, "criticsid");
            return (Criteria) this;
        }

        public Criteria andBecriticsidIsNull() {
            addCriterion("becriticsid is null");
            return (Criteria) this;
        }

        public Criteria andBecriticsidIsNotNull() {
            addCriterion("becriticsid is not null");
            return (Criteria) this;
        }

        public Criteria andBecriticsidEqualTo(Integer value) {
            addCriterion("becriticsid =", value, "becriticsid");
            return (Criteria) this;
        }

        public Criteria andBecriticsidNotEqualTo(Integer value) {
            addCriterion("becriticsid <>", value, "becriticsid");
            return (Criteria) this;
        }

        public Criteria andBecriticsidGreaterThan(Integer value) {
            addCriterion("becriticsid >", value, "becriticsid");
            return (Criteria) this;
        }

        public Criteria andBecriticsidGreaterThanOrEqualTo(Integer value) {
            addCriterion("becriticsid >=", value, "becriticsid");
            return (Criteria) this;
        }

        public Criteria andBecriticsidLessThan(Integer value) {
            addCriterion("becriticsid <", value, "becriticsid");
            return (Criteria) this;
        }

        public Criteria andBecriticsidLessThanOrEqualTo(Integer value) {
            addCriterion("becriticsid <=", value, "becriticsid");
            return (Criteria) this;
        }

        public Criteria andBecriticsidIn(List<Integer> values) {
            addCriterion("becriticsid in", values, "becriticsid");
            return (Criteria) this;
        }

        public Criteria andBecriticsidNotIn(List<Integer> values) {
            addCriterion("becriticsid not in", values, "becriticsid");
            return (Criteria) this;
        }

        public Criteria andBecriticsidBetween(Integer value1, Integer value2) {
            addCriterion("becriticsid between", value1, value2, "becriticsid");
            return (Criteria) this;
        }

        public Criteria andBecriticsidNotBetween(Integer value1, Integer value2) {
            addCriterion("becriticsid not between", value1, value2, "becriticsid");
            return (Criteria) this;
        }

        public Criteria andContentIsNull() {
            addCriterion("content is null");
            return (Criteria) this;
        }

        public Criteria andContentIsNotNull() {
            addCriterion("content is not null");
            return (Criteria) this;
        }

        public Criteria andContentEqualTo(String value) {
            addCriterion("content =", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentNotEqualTo(String value) {
            addCriterion("content <>", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentGreaterThan(String value) {
            addCriterion("content >", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentGreaterThanOrEqualTo(String value) {
            addCriterion("content >=", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentLessThan(String value) {
            addCriterion("content <", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentLessThanOrEqualTo(String value) {
            addCriterion("content <=", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentLike(String value) {
            addCriterion("content like", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentNotLike(String value) {
            addCriterion("content not like", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentIn(List<String> values) {
            addCriterion("content in", values, "content");
            return (Criteria) this;
        }

        public Criteria andContentNotIn(List<String> values) {
            addCriterion("content not in", values, "content");
            return (Criteria) this;
        }

        public Criteria andContentBetween(String value1, String value2) {
            addCriterion("content between", value1, value2, "content");
            return (Criteria) this;
        }

        public Criteria andContentNotBetween(String value1, String value2) {
            addCriterion("content not between", value1, value2, "content");
            return (Criteria) this;
        }

        public Criteria andBearticleidIsNull() {
            addCriterion("bearticleid is null");
            return (Criteria) this;
        }

        public Criteria andBearticleidIsNotNull() {
            addCriterion("bearticleid is not null");
            return (Criteria) this;
        }

        public Criteria andBearticleidEqualTo(Integer value) {
            addCriterion("bearticleid =", value, "bearticleid");
            return (Criteria) this;
        }

        public Criteria andBearticleidNotEqualTo(Integer value) {
            addCriterion("bearticleid <>", value, "bearticleid");
            return (Criteria) this;
        }

        public Criteria andBearticleidGreaterThan(Integer value) {
            addCriterion("bearticleid >", value, "bearticleid");
            return (Criteria) this;
        }

        public Criteria andBearticleidGreaterThanOrEqualTo(Integer value) {
            addCriterion("bearticleid >=", value, "bearticleid");
            return (Criteria) this;
        }

        public Criteria andBearticleidLessThan(Integer value) {
            addCriterion("bearticleid <", value, "bearticleid");
            return (Criteria) this;
        }

        public Criteria andBearticleidLessThanOrEqualTo(Integer value) {
            addCriterion("bearticleid <=", value, "bearticleid");
            return (Criteria) this;
        }

        public Criteria andBearticleidIn(List<Integer> values) {
            addCriterion("bearticleid in", values, "bearticleid");
            return (Criteria) this;
        }

        public Criteria andBearticleidNotIn(List<Integer> values) {
            addCriterion("bearticleid not in", values, "bearticleid");
            return (Criteria) this;
        }

        public Criteria andBearticleidBetween(Integer value1, Integer value2) {
            addCriterion("bearticleid between", value1, value2, "bearticleid");
            return (Criteria) this;
        }

        public Criteria andBearticleidNotBetween(Integer value1, Integer value2) {
            addCriterion("bearticleid not between", value1, value2, "bearticleid");
            return (Criteria) this;
        }

        public Criteria andIsanswerIsNull() {
            addCriterion("isanswer is null");
            return (Criteria) this;
        }

        public Criteria andIsanswerIsNotNull() {
            addCriterion("isanswer is not null");
            return (Criteria) this;
        }

        public Criteria andIsanswerEqualTo(Integer value) {
            addCriterion("isanswer =", value, "isanswer");
            return (Criteria) this;
        }

        public Criteria andIsanswerNotEqualTo(Integer value) {
            addCriterion("isanswer <>", value, "isanswer");
            return (Criteria) this;
        }

        public Criteria andIsanswerGreaterThan(Integer value) {
            addCriterion("isanswer >", value, "isanswer");
            return (Criteria) this;
        }

        public Criteria andIsanswerGreaterThanOrEqualTo(Integer value) {
            addCriterion("isanswer >=", value, "isanswer");
            return (Criteria) this;
        }

        public Criteria andIsanswerLessThan(Integer value) {
            addCriterion("isanswer <", value, "isanswer");
            return (Criteria) this;
        }

        public Criteria andIsanswerLessThanOrEqualTo(Integer value) {
            addCriterion("isanswer <=", value, "isanswer");
            return (Criteria) this;
        }

        public Criteria andIsanswerIn(List<Integer> values) {
            addCriterion("isanswer in", values, "isanswer");
            return (Criteria) this;
        }

        public Criteria andIsanswerNotIn(List<Integer> values) {
            addCriterion("isanswer not in", values, "isanswer");
            return (Criteria) this;
        }

        public Criteria andIsanswerBetween(Integer value1, Integer value2) {
            addCriterion("isanswer between", value1, value2, "isanswer");
            return (Criteria) this;
        }

        public Criteria andIsanswerNotBetween(Integer value1, Integer value2) {
            addCriterion("isanswer not between", value1, value2, "isanswer");
            return (Criteria) this;
        }

        public Criteria andCommenttimeIsNull() {
            addCriterion("commenttime is null");
            return (Criteria) this;
        }

        public Criteria andCommenttimeIsNotNull() {
            addCriterion("commenttime is not null");
            return (Criteria) this;
        }

        public Criteria andCommenttimeEqualTo(Date value) {
            addCriterion("commenttime =", value, "commenttime");
            return (Criteria) this;
        }

        public Criteria andCommenttimeNotEqualTo(Date value) {
            addCriterion("commenttime <>", value, "commenttime");
            return (Criteria) this;
        }

        public Criteria andCommenttimeGreaterThan(Date value) {
            addCriterion("commenttime >", value, "commenttime");
            return (Criteria) this;
        }

        public Criteria andCommenttimeGreaterThanOrEqualTo(Date value) {
            addCriterion("commenttime >=", value, "commenttime");
            return (Criteria) this;
        }

        public Criteria andCommenttimeLessThan(Date value) {
            addCriterion("commenttime <", value, "commenttime");
            return (Criteria) this;
        }

        public Criteria andCommenttimeLessThanOrEqualTo(Date value) {
            addCriterion("commenttime <=", value, "commenttime");
            return (Criteria) this;
        }

        public Criteria andCommenttimeIn(List<Date> values) {
            addCriterion("commenttime in", values, "commenttime");
            return (Criteria) this;
        }

        public Criteria andCommenttimeNotIn(List<Date> values) {
            addCriterion("commenttime not in", values, "commenttime");
            return (Criteria) this;
        }

        public Criteria andCommenttimeBetween(Date value1, Date value2) {
            addCriterion("commenttime between", value1, value2, "commenttime");
            return (Criteria) this;
        }

        public Criteria andCommenttimeNotBetween(Date value1, Date value2) {
            addCriterion("commenttime not between", value1, value2, "commenttime");
            return (Criteria) this;
        }

        public Criteria andAnsweridIsNull() {
            addCriterion("answerid is null");
            return (Criteria) this;
        }

        public Criteria andAnsweridIsNotNull() {
            addCriterion("answerid is not null");
            return (Criteria) this;
        }

        public Criteria andAnsweridEqualTo(Integer value) {
            addCriterion("answerid =", value, "answerid");
            return (Criteria) this;
        }

        public Criteria andAnsweridNotEqualTo(Integer value) {
            addCriterion("answerid <>", value, "answerid");
            return (Criteria) this;
        }

        public Criteria andAnsweridGreaterThan(Integer value) {
            addCriterion("answerid >", value, "answerid");
            return (Criteria) this;
        }

        public Criteria andAnsweridGreaterThanOrEqualTo(Integer value) {
            addCriterion("answerid >=", value, "answerid");
            return (Criteria) this;
        }

        public Criteria andAnsweridLessThan(Integer value) {
            addCriterion("answerid <", value, "answerid");
            return (Criteria) this;
        }

        public Criteria andAnsweridLessThanOrEqualTo(Integer value) {
            addCriterion("answerid <=", value, "answerid");
            return (Criteria) this;
        }

        public Criteria andAnsweridIn(List<Integer> values) {
            addCriterion("answerid in", values, "answerid");
            return (Criteria) this;
        }

        public Criteria andAnsweridNotIn(List<Integer> values) {
            addCriterion("answerid not in", values, "answerid");
            return (Criteria) this;
        }

        public Criteria andAnsweridBetween(Integer value1, Integer value2) {
            addCriterion("answerid between", value1, value2, "answerid");
            return (Criteria) this;
        }

        public Criteria andAnsweridNotBetween(Integer value1, Integer value2) {
            addCriterion("answerid not between", value1, value2, "answerid");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}