package com.phoenix.zhihu.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ArcticExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public ArcticExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andArticlenameIsNull() {
            addCriterion("articlename is null");
            return (Criteria) this;
        }

        public Criteria andArticlenameIsNotNull() {
            addCriterion("articlename is not null");
            return (Criteria) this;
        }

        public Criteria andArticlenameEqualTo(String value) {
            addCriterion("articlename =", value, "articlename");
            return (Criteria) this;
        }

        public Criteria andArticlenameNotEqualTo(String value) {
            addCriterion("articlename <>", value, "articlename");
            return (Criteria) this;
        }

        public Criteria andArticlenameGreaterThan(String value) {
            addCriterion("articlename >", value, "articlename");
            return (Criteria) this;
        }

        public Criteria andArticlenameGreaterThanOrEqualTo(String value) {
            addCriterion("articlename >=", value, "articlename");
            return (Criteria) this;
        }

        public Criteria andArticlenameLessThan(String value) {
            addCriterion("articlename <", value, "articlename");
            return (Criteria) this;
        }

        public Criteria andArticlenameLessThanOrEqualTo(String value) {
            addCriterion("articlename <=", value, "articlename");
            return (Criteria) this;
        }

        public Criteria andArticlenameLike(String value) {
            addCriterion("articlename like", value, "articlename");
            return (Criteria) this;
        }

        public Criteria andArticlenameNotLike(String value) {
            addCriterion("articlename not like", value, "articlename");
            return (Criteria) this;
        }

        public Criteria andArticlenameIn(List<String> values) {
            addCriterion("articlename in", values, "articlename");
            return (Criteria) this;
        }

        public Criteria andArticlenameNotIn(List<String> values) {
            addCriterion("articlename not in", values, "articlename");
            return (Criteria) this;
        }

        public Criteria andArticlenameBetween(String value1, String value2) {
            addCriterion("articlename between", value1, value2, "articlename");
            return (Criteria) this;
        }

        public Criteria andArticlenameNotBetween(String value1, String value2) {
            addCriterion("articlename not between", value1, value2, "articlename");
            return (Criteria) this;
        }

        public Criteria andAuthoridIsNull() {
            addCriterion("authorid is null");
            return (Criteria) this;
        }

        public Criteria andAuthoridIsNotNull() {
            addCriterion("authorid is not null");
            return (Criteria) this;
        }

        public Criteria andAuthoridEqualTo(Integer value) {
            addCriterion("authorid =", value, "authorid");
            return (Criteria) this;
        }

        public Criteria andAuthoridNotEqualTo(Integer value) {
            addCriterion("authorid <>", value, "authorid");
            return (Criteria) this;
        }

        public Criteria andAuthoridGreaterThan(Integer value) {
            addCriterion("authorid >", value, "authorid");
            return (Criteria) this;
        }

        public Criteria andAuthoridGreaterThanOrEqualTo(Integer value) {
            addCriterion("authorid >=", value, "authorid");
            return (Criteria) this;
        }

        public Criteria andAuthoridLessThan(Integer value) {
            addCriterion("authorid <", value, "authorid");
            return (Criteria) this;
        }

        public Criteria andAuthoridLessThanOrEqualTo(Integer value) {
            addCriterion("authorid <=", value, "authorid");
            return (Criteria) this;
        }

        public Criteria andAuthoridIn(List<Integer> values) {
            addCriterion("authorid in", values, "authorid");
            return (Criteria) this;
        }

        public Criteria andAuthoridNotIn(List<Integer> values) {
            addCriterion("authorid not in", values, "authorid");
            return (Criteria) this;
        }

        public Criteria andAuthoridBetween(Integer value1, Integer value2) {
            addCriterion("authorid between", value1, value2, "authorid");
            return (Criteria) this;
        }

        public Criteria andAuthoridNotBetween(Integer value1, Integer value2) {
            addCriterion("authorid not between", value1, value2, "authorid");
            return (Criteria) this;
        }

        public Criteria andCreatetimeIsNull() {
            addCriterion("createtime is null");
            return (Criteria) this;
        }

        public Criteria andCreatetimeIsNotNull() {
            addCriterion("createtime is not null");
            return (Criteria) this;
        }

        public Criteria andCreatetimeEqualTo(Date value) {
            addCriterion("createtime =", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeNotEqualTo(Date value) {
            addCriterion("createtime <>", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeGreaterThan(Date value) {
            addCriterion("createtime >", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeGreaterThanOrEqualTo(Date value) {
            addCriterion("createtime >=", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeLessThan(Date value) {
            addCriterion("createtime <", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeLessThanOrEqualTo(Date value) {
            addCriterion("createtime <=", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeIn(List<Date> values) {
            addCriterion("createtime in", values, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeNotIn(List<Date> values) {
            addCriterion("createtime not in", values, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeBetween(Date value1, Date value2) {
            addCriterion("createtime between", value1, value2, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeNotBetween(Date value1, Date value2) {
            addCriterion("createtime not between", value1, value2, "createtime");
            return (Criteria) this;
        }

        public Criteria andAgreeIsNull() {
            addCriterion("agree is null");
            return (Criteria) this;
        }

        public Criteria andAgreeIsNotNull() {
            addCriterion("agree is not null");
            return (Criteria) this;
        }

        public Criteria andAgreeEqualTo(Integer value) {
            addCriterion("agree =", value, "agree");
            return (Criteria) this;
        }

        public Criteria andAgreeNotEqualTo(Integer value) {
            addCriterion("agree <>", value, "agree");
            return (Criteria) this;
        }

        public Criteria andAgreeGreaterThan(Integer value) {
            addCriterion("agree >", value, "agree");
            return (Criteria) this;
        }

        public Criteria andAgreeGreaterThanOrEqualTo(Integer value) {
            addCriterion("agree >=", value, "agree");
            return (Criteria) this;
        }

        public Criteria andAgreeLessThan(Integer value) {
            addCriterion("agree <", value, "agree");
            return (Criteria) this;
        }

        public Criteria andAgreeLessThanOrEqualTo(Integer value) {
            addCriterion("agree <=", value, "agree");
            return (Criteria) this;
        }

        public Criteria andAgreeIn(List<Integer> values) {
            addCriterion("agree in", values, "agree");
            return (Criteria) this;
        }

        public Criteria andAgreeNotIn(List<Integer> values) {
            addCriterion("agree not in", values, "agree");
            return (Criteria) this;
        }

        public Criteria andAgreeBetween(Integer value1, Integer value2) {
            addCriterion("agree between", value1, value2, "agree");
            return (Criteria) this;
        }

        public Criteria andAgreeNotBetween(Integer value1, Integer value2) {
            addCriterion("agree not between", value1, value2, "agree");
            return (Criteria) this;
        }

        public Criteria andCommentnumberIsNull() {
            addCriterion("commentnumber is null");
            return (Criteria) this;
        }

        public Criteria andCommentnumberIsNotNull() {
            addCriterion("commentnumber is not null");
            return (Criteria) this;
        }

        public Criteria andCommentnumberEqualTo(Integer value) {
            addCriterion("commentnumber =", value, "commentnumber");
            return (Criteria) this;
        }

        public Criteria andCommentnumberNotEqualTo(Integer value) {
            addCriterion("commentnumber <>", value, "commentnumber");
            return (Criteria) this;
        }

        public Criteria andCommentnumberGreaterThan(Integer value) {
            addCriterion("commentnumber >", value, "commentnumber");
            return (Criteria) this;
        }

        public Criteria andCommentnumberGreaterThanOrEqualTo(Integer value) {
            addCriterion("commentnumber >=", value, "commentnumber");
            return (Criteria) this;
        }

        public Criteria andCommentnumberLessThan(Integer value) {
            addCriterion("commentnumber <", value, "commentnumber");
            return (Criteria) this;
        }

        public Criteria andCommentnumberLessThanOrEqualTo(Integer value) {
            addCriterion("commentnumber <=", value, "commentnumber");
            return (Criteria) this;
        }

        public Criteria andCommentnumberIn(List<Integer> values) {
            addCriterion("commentnumber in", values, "commentnumber");
            return (Criteria) this;
        }

        public Criteria andCommentnumberNotIn(List<Integer> values) {
            addCriterion("commentnumber not in", values, "commentnumber");
            return (Criteria) this;
        }

        public Criteria andCommentnumberBetween(Integer value1, Integer value2) {
            addCriterion("commentnumber between", value1, value2, "commentnumber");
            return (Criteria) this;
        }

        public Criteria andCommentnumberNotBetween(Integer value1, Integer value2) {
            addCriterion("commentnumber not between", value1, value2, "commentnumber");
            return (Criteria) this;
        }

        public Criteria andArticlepictureIsNull() {
            addCriterion("articlepicture is null");
            return (Criteria) this;
        }

        public Criteria andArticlepictureIsNotNull() {
            addCriterion("articlepicture is not null");
            return (Criteria) this;
        }

        public Criteria andArticlepictureEqualTo(String value) {
            addCriterion("articlepicture =", value, "articlepicture");
            return (Criteria) this;
        }

        public Criteria andArticlepictureNotEqualTo(String value) {
            addCriterion("articlepicture <>", value, "articlepicture");
            return (Criteria) this;
        }

        public Criteria andArticlepictureGreaterThan(String value) {
            addCriterion("articlepicture >", value, "articlepicture");
            return (Criteria) this;
        }

        public Criteria andArticlepictureGreaterThanOrEqualTo(String value) {
            addCriterion("articlepicture >=", value, "articlepicture");
            return (Criteria) this;
        }

        public Criteria andArticlepictureLessThan(String value) {
            addCriterion("articlepicture <", value, "articlepicture");
            return (Criteria) this;
        }

        public Criteria andArticlepictureLessThanOrEqualTo(String value) {
            addCriterion("articlepicture <=", value, "articlepicture");
            return (Criteria) this;
        }

        public Criteria andArticlepictureLike(String value) {
            addCriterion("articlepicture like", value, "articlepicture");
            return (Criteria) this;
        }

        public Criteria andArticlepictureNotLike(String value) {
            addCriterion("articlepicture not like", value, "articlepicture");
            return (Criteria) this;
        }

        public Criteria andArticlepictureIn(List<String> values) {
            addCriterion("articlepicture in", values, "articlepicture");
            return (Criteria) this;
        }

        public Criteria andArticlepictureNotIn(List<String> values) {
            addCriterion("articlepicture not in", values, "articlepicture");
            return (Criteria) this;
        }

        public Criteria andArticlepictureBetween(String value1, String value2) {
            addCriterion("articlepicture between", value1, value2, "articlepicture");
            return (Criteria) this;
        }

        public Criteria andArticlepictureNotBetween(String value1, String value2) {
            addCriterion("articlepicture not between", value1, value2, "articlepicture");
            return (Criteria) this;
        }

        public Criteria andColumnidIsNull() {
            addCriterion("columnid is null");
            return (Criteria) this;
        }

        public Criteria andColumnidIsNotNull() {
            addCriterion("columnid is not null");
            return (Criteria) this;
        }

        public Criteria andColumnidEqualTo(Integer value) {
            addCriterion("columnid =", value, "columnid");
            return (Criteria) this;
        }

        public Criteria andColumnidNotEqualTo(Integer value) {
            addCriterion("columnid <>", value, "columnid");
            return (Criteria) this;
        }

        public Criteria andColumnidGreaterThan(Integer value) {
            addCriterion("columnid >", value, "columnid");
            return (Criteria) this;
        }

        public Criteria andColumnidGreaterThanOrEqualTo(Integer value) {
            addCriterion("columnid >=", value, "columnid");
            return (Criteria) this;
        }

        public Criteria andColumnidLessThan(Integer value) {
            addCriterion("columnid <", value, "columnid");
            return (Criteria) this;
        }

        public Criteria andColumnidLessThanOrEqualTo(Integer value) {
            addCriterion("columnid <=", value, "columnid");
            return (Criteria) this;
        }

        public Criteria andColumnidIn(List<Integer> values) {
            addCriterion("columnid in", values, "columnid");
            return (Criteria) this;
        }

        public Criteria andColumnidNotIn(List<Integer> values) {
            addCriterion("columnid not in", values, "columnid");
            return (Criteria) this;
        }

        public Criteria andColumnidBetween(Integer value1, Integer value2) {
            addCriterion("columnid between", value1, value2, "columnid");
            return (Criteria) this;
        }

        public Criteria andColumnidNotBetween(Integer value1, Integer value2) {
            addCriterion("columnid not between", value1, value2, "columnid");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}