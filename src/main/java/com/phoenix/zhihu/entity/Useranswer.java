package com.phoenix.zhihu.entity;

import java.util.Date;

public class Useranswer {
    private Integer id;

    private Integer answerid;

    private Integer userid;

    private Integer userattitude;

    private Date attitudetime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAnswerid() {
        return answerid;
    }

    public void setAnswerid(Integer answerid) {
        this.answerid = answerid;
    }

    public Integer getUserid() {
        return userid;
    }

    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    public Integer getUserattitude() {
        return userattitude;
    }

    public void setUserattitude(Integer userattitude) {
        this.userattitude = userattitude;
    }

    public Date getAttitudetime() {
        return attitudetime;
    }

    public void setAttitudetime(Date attitudetime) {
        this.attitudetime = attitudetime;
    }
}