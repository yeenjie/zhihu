package com.phoenix.zhihu.entity;

public class Collectionnew {

    private long id;
    private long userid;
    private long answerid;

    public long getId() {

        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getUserid() {
        return userid;
    }

    public void setUserid(long userid) {
        this.userid = userid;
    }

    public long getAnswerid() {
        return answerid;
    }

    public void setAnswerid(long answerid) {
        this.answerid = answerid;
    }
}
