package com.phoenix.zhihu.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CollectionarticleExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public CollectionarticleExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andCollectionidIsNull() {
            addCriterion("collectionid is null");
            return (Criteria) this;
        }

        public Criteria andCollectionidIsNotNull() {
            addCriterion("collectionid is not null");
            return (Criteria) this;
        }

        public Criteria andCollectionidEqualTo(Integer value) {
            addCriterion("collectionid =", value, "collectionid");
            return (Criteria) this;
        }

        public Criteria andCollectionidNotEqualTo(Integer value) {
            addCriterion("collectionid <>", value, "collectionid");
            return (Criteria) this;
        }

        public Criteria andCollectionidGreaterThan(Integer value) {
            addCriterion("collectionid >", value, "collectionid");
            return (Criteria) this;
        }

        public Criteria andCollectionidGreaterThanOrEqualTo(Integer value) {
            addCriterion("collectionid >=", value, "collectionid");
            return (Criteria) this;
        }

        public Criteria andCollectionidLessThan(Integer value) {
            addCriterion("collectionid <", value, "collectionid");
            return (Criteria) this;
        }

        public Criteria andCollectionidLessThanOrEqualTo(Integer value) {
            addCriterion("collectionid <=", value, "collectionid");
            return (Criteria) this;
        }

        public Criteria andCollectionidIn(List<Integer> values) {
            addCriterion("collectionid in", values, "collectionid");
            return (Criteria) this;
        }

        public Criteria andCollectionidNotIn(List<Integer> values) {
            addCriterion("collectionid not in", values, "collectionid");
            return (Criteria) this;
        }

        public Criteria andCollectionidBetween(Integer value1, Integer value2) {
            addCriterion("collectionid between", value1, value2, "collectionid");
            return (Criteria) this;
        }

        public Criteria andCollectionidNotBetween(Integer value1, Integer value2) {
            addCriterion("collectionid not between", value1, value2, "collectionid");
            return (Criteria) this;
        }

        public Criteria andArticleidIsNull() {
            addCriterion("articleid is null");
            return (Criteria) this;
        }

        public Criteria andArticleidIsNotNull() {
            addCriterion("articleid is not null");
            return (Criteria) this;
        }

        public Criteria andArticleidEqualTo(Integer value) {
            addCriterion("articleid =", value, "articleid");
            return (Criteria) this;
        }

        public Criteria andArticleidNotEqualTo(Integer value) {
            addCriterion("articleid <>", value, "articleid");
            return (Criteria) this;
        }

        public Criteria andArticleidGreaterThan(Integer value) {
            addCriterion("articleid >", value, "articleid");
            return (Criteria) this;
        }

        public Criteria andArticleidGreaterThanOrEqualTo(Integer value) {
            addCriterion("articleid >=", value, "articleid");
            return (Criteria) this;
        }

        public Criteria andArticleidLessThan(Integer value) {
            addCriterion("articleid <", value, "articleid");
            return (Criteria) this;
        }

        public Criteria andArticleidLessThanOrEqualTo(Integer value) {
            addCriterion("articleid <=", value, "articleid");
            return (Criteria) this;
        }

        public Criteria andArticleidIn(List<Integer> values) {
            addCriterion("articleid in", values, "articleid");
            return (Criteria) this;
        }

        public Criteria andArticleidNotIn(List<Integer> values) {
            addCriterion("articleid not in", values, "articleid");
            return (Criteria) this;
        }

        public Criteria andArticleidBetween(Integer value1, Integer value2) {
            addCriterion("articleid between", value1, value2, "articleid");
            return (Criteria) this;
        }

        public Criteria andArticleidNotBetween(Integer value1, Integer value2) {
            addCriterion("articleid not between", value1, value2, "articleid");
            return (Criteria) this;
        }

        public Criteria andActiclenameIsNull() {
            addCriterion("acticlename is null");
            return (Criteria) this;
        }

        public Criteria andActiclenameIsNotNull() {
            addCriterion("acticlename is not null");
            return (Criteria) this;
        }

        public Criteria andActiclenameEqualTo(String value) {
            addCriterion("acticlename =", value, "acticlename");
            return (Criteria) this;
        }

        public Criteria andActiclenameNotEqualTo(String value) {
            addCriterion("acticlename <>", value, "acticlename");
            return (Criteria) this;
        }

        public Criteria andActiclenameGreaterThan(String value) {
            addCriterion("acticlename >", value, "acticlename");
            return (Criteria) this;
        }

        public Criteria andActiclenameGreaterThanOrEqualTo(String value) {
            addCriterion("acticlename >=", value, "acticlename");
            return (Criteria) this;
        }

        public Criteria andActiclenameLessThan(String value) {
            addCriterion("acticlename <", value, "acticlename");
            return (Criteria) this;
        }

        public Criteria andActiclenameLessThanOrEqualTo(String value) {
            addCriterion("acticlename <=", value, "acticlename");
            return (Criteria) this;
        }

        public Criteria andActiclenameLike(String value) {
            addCriterion("acticlename like", value, "acticlename");
            return (Criteria) this;
        }

        public Criteria andActiclenameNotLike(String value) {
            addCriterion("acticlename not like", value, "acticlename");
            return (Criteria) this;
        }

        public Criteria andActiclenameIn(List<String> values) {
            addCriterion("acticlename in", values, "acticlename");
            return (Criteria) this;
        }

        public Criteria andActiclenameNotIn(List<String> values) {
            addCriterion("acticlename not in", values, "acticlename");
            return (Criteria) this;
        }

        public Criteria andActiclenameBetween(String value1, String value2) {
            addCriterion("acticlename between", value1, value2, "acticlename");
            return (Criteria) this;
        }

        public Criteria andActiclenameNotBetween(String value1, String value2) {
            addCriterion("acticlename not between", value1, value2, "acticlename");
            return (Criteria) this;
        }

        public Criteria andCollectiontimeIsNull() {
            addCriterion("collectiontime is null");
            return (Criteria) this;
        }

        public Criteria andCollectiontimeIsNotNull() {
            addCriterion("collectiontime is not null");
            return (Criteria) this;
        }

        public Criteria andCollectiontimeEqualTo(Date value) {
            addCriterion("collectiontime =", value, "collectiontime");
            return (Criteria) this;
        }

        public Criteria andCollectiontimeNotEqualTo(Date value) {
            addCriterion("collectiontime <>", value, "collectiontime");
            return (Criteria) this;
        }

        public Criteria andCollectiontimeGreaterThan(Date value) {
            addCriterion("collectiontime >", value, "collectiontime");
            return (Criteria) this;
        }

        public Criteria andCollectiontimeGreaterThanOrEqualTo(Date value) {
            addCriterion("collectiontime >=", value, "collectiontime");
            return (Criteria) this;
        }

        public Criteria andCollectiontimeLessThan(Date value) {
            addCriterion("collectiontime <", value, "collectiontime");
            return (Criteria) this;
        }

        public Criteria andCollectiontimeLessThanOrEqualTo(Date value) {
            addCriterion("collectiontime <=", value, "collectiontime");
            return (Criteria) this;
        }

        public Criteria andCollectiontimeIn(List<Date> values) {
            addCriterion("collectiontime in", values, "collectiontime");
            return (Criteria) this;
        }

        public Criteria andCollectiontimeNotIn(List<Date> values) {
            addCriterion("collectiontime not in", values, "collectiontime");
            return (Criteria) this;
        }

        public Criteria andCollectiontimeBetween(Date value1, Date value2) {
            addCriterion("collectiontime between", value1, value2, "collectiontime");
            return (Criteria) this;
        }

        public Criteria andCollectiontimeNotBetween(Date value1, Date value2) {
            addCriterion("collectiontime not between", value1, value2, "collectiontime");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}