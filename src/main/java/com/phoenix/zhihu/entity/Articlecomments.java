package com.phoenix.zhihu.entity;

import java.util.Date;

public class Articlecomments {
    private Integer id;

    private Integer criticsid;

    private Integer becriticsid;

    private String content;

    private Integer bearticleid;

    private Integer isarticle;

    private Date time;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCriticsid() {
        return criticsid;
    }

    public void setCriticsid(Integer criticsid) {
        this.criticsid = criticsid;
    }

    public Integer getBecriticsid() {
        return becriticsid;
    }

    public void setBecriticsid(Integer becriticsid) {
        this.becriticsid = becriticsid;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }

    public Integer getBearticleid() {
        return bearticleid;
    }

    public void setBearticleid(Integer bearticleid) {
        this.bearticleid = bearticleid;
    }

    public Integer getIsarticle() {
        return isarticle;
    }

    public void setIsarticle(Integer isarticle) {
        this.isarticle = isarticle;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }
}