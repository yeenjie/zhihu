package com.phoenix.zhihu.entity;

import java.util.Date;

public class Attentioncolumn {
    private Integer id;

    private Integer userid;

    private Integer columnid;

    private Integer authorid;

    private Date attentiontime;

    private String column;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserid() {
        return userid;
    }

    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    public Integer getColumnid() {
        return columnid;
    }

    public void setColumnid(Integer columnid) {
        this.columnid = columnid;
    }

    public Integer getAuthorid() {
        return authorid;
    }

    public void setAuthorid(Integer authorid) {
        this.authorid = authorid;
    }

    public Date getAttentiontime() {
        return attentiontime;
    }

    public void setAttentiontime(Date attentiontime) {
        this.attentiontime = attentiontime;
    }

    public String getColumn() {
        return column;
    }

    public void setColumn(String column) {
        this.column = column == null ? null : column.trim();
    }
}