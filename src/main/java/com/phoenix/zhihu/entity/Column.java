package com.phoenix.zhihu.entity;

import java.util.Date;

public class Column {
    private Integer id;

    private String columnname;

    private Integer authorid;

    private Date createtime;

    private String columndescribe;

    private Integer reportnumber;

    private Integer attentionnumber;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getColumnname() {
        return columnname;
    }

    public void setColumnname(String columnname) {
        this.columnname = columnname == null ? null : columnname.trim();
    }

    public Integer getAuthorid() {
        return authorid;
    }

    public void setAuthorid(Integer authorid) {
        this.authorid = authorid;
    }

    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    public String getColumndescribe() {
        return columndescribe;
    }

    public void setColumndescribe(String columndescribe) {
        this.columndescribe = columndescribe == null ? null : columndescribe.trim();
    }

    public Integer getReportnumber() {
        return reportnumber;
    }

    public void setReportnumber(Integer reportnumber) {
        this.reportnumber = reportnumber;
    }

    public Integer getAttentionnumber() {
        return attentionnumber;
    }

    public void setAttentionnumber(Integer attentionnumber) {
        this.attentionnumber = attentionnumber;
    }
}