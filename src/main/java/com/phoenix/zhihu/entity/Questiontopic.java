package com.phoenix.zhihu.entity;

public class Questiontopic {
    private Integer id;

    private Integer questionid;

    private Integer topicid;

    private Integer topiccategoryid;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getQuestionid() {
        return questionid;
    }

    public void setQuestionid(Integer questionid) {
        this.questionid = questionid;
    }

    public Integer getTopicid() {
        return topicid;
    }

    public void setTopicid(Integer topicid) {
        this.topicid = topicid;
    }

    public Integer getTopiccategoryid() {
        return topiccategoryid;
    }

    public void setTopiccategoryid(Integer topiccategoryid) {
        this.topiccategoryid = topiccategoryid;
    }
}