package com.phoenix.zhihu.entity;

import java.util.Date;

public class Collectionarticle {
    private Integer id;

    private Integer collectionid;

    private Integer articleid;

    private String acticlename;

    private Date collectiontime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCollectionid() {
        return collectionid;
    }

    public void setCollectionid(Integer collectionid) {
        this.collectionid = collectionid;
    }

    public Integer getArticleid() {
        return articleid;
    }

    public void setArticleid(Integer articleid) {
        this.articleid = articleid;
    }

    public String getActiclename() {
        return acticlename;
    }

    public void setActiclename(String acticlename) {
        this.acticlename = acticlename == null ? null : acticlename.trim();
    }

    public Date getCollectiontime() {
        return collectiontime;
    }

    public void setCollectiontime(Date collectiontime) {
        this.collectiontime = collectiontime;
    }
}