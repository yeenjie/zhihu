package com.phoenix.zhihu.entity;

import java.util.Date;

public class Answercomment {
    private Integer id;

    private Integer criticsid;

    private Integer becriticsid;

    private String content;

    private Integer bearticleid;

    private Integer isanswer;

    private Date commenttime;

    private Integer answerid;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCriticsid() {
        return criticsid;
    }

    public void setCriticsid(Integer criticsid) {
        this.criticsid = criticsid;
    }

    public Integer getBecriticsid() {
        return becriticsid;
    }

    public void setBecriticsid(Integer becriticsid) {
        this.becriticsid = becriticsid;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }

    public Integer getBearticleid() {
        return bearticleid;
    }

    public void setBearticleid(Integer bearticleid) {
        this.bearticleid = bearticleid;
    }

    public Integer getIsanswer() {
        return isanswer;
    }

    public void setIsanswer(Integer isanswer) {
        this.isanswer = isanswer;
    }

    public Date getCommenttime() {
        return commenttime;
    }

    public void setCommenttime(Date commenttime) {
        this.commenttime = commenttime;
    }

    public Integer getAnswerid() {
        return answerid;
    }

    public void setAnswerid(Integer answerid) {
        this.answerid = answerid;
    }
}