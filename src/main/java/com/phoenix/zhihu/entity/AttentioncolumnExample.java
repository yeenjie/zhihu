package com.phoenix.zhihu.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class AttentioncolumnExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public AttentioncolumnExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andUseridIsNull() {
            addCriterion("userid is null");
            return (Criteria) this;
        }

        public Criteria andUseridIsNotNull() {
            addCriterion("userid is not null");
            return (Criteria) this;
        }

        public Criteria andUseridEqualTo(Integer value) {
            addCriterion("userid =", value, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridNotEqualTo(Integer value) {
            addCriterion("userid <>", value, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridGreaterThan(Integer value) {
            addCriterion("userid >", value, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridGreaterThanOrEqualTo(Integer value) {
            addCriterion("userid >=", value, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridLessThan(Integer value) {
            addCriterion("userid <", value, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridLessThanOrEqualTo(Integer value) {
            addCriterion("userid <=", value, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridIn(List<Integer> values) {
            addCriterion("userid in", values, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridNotIn(List<Integer> values) {
            addCriterion("userid not in", values, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridBetween(Integer value1, Integer value2) {
            addCriterion("userid between", value1, value2, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridNotBetween(Integer value1, Integer value2) {
            addCriterion("userid not between", value1, value2, "userid");
            return (Criteria) this;
        }

        public Criteria andColumnidIsNull() {
            addCriterion("columnid is null");
            return (Criteria) this;
        }

        public Criteria andColumnidIsNotNull() {
            addCriterion("columnid is not null");
            return (Criteria) this;
        }

        public Criteria andColumnidEqualTo(Integer value) {
            addCriterion("columnid =", value, "columnid");
            return (Criteria) this;
        }

        public Criteria andColumnidNotEqualTo(Integer value) {
            addCriterion("columnid <>", value, "columnid");
            return (Criteria) this;
        }

        public Criteria andColumnidGreaterThan(Integer value) {
            addCriterion("columnid >", value, "columnid");
            return (Criteria) this;
        }

        public Criteria andColumnidGreaterThanOrEqualTo(Integer value) {
            addCriterion("columnid >=", value, "columnid");
            return (Criteria) this;
        }

        public Criteria andColumnidLessThan(Integer value) {
            addCriterion("columnid <", value, "columnid");
            return (Criteria) this;
        }

        public Criteria andColumnidLessThanOrEqualTo(Integer value) {
            addCriterion("columnid <=", value, "columnid");
            return (Criteria) this;
        }

        public Criteria andColumnidIn(List<Integer> values) {
            addCriterion("columnid in", values, "columnid");
            return (Criteria) this;
        }

        public Criteria andColumnidNotIn(List<Integer> values) {
            addCriterion("columnid not in", values, "columnid");
            return (Criteria) this;
        }

        public Criteria andColumnidBetween(Integer value1, Integer value2) {
            addCriterion("columnid between", value1, value2, "columnid");
            return (Criteria) this;
        }

        public Criteria andColumnidNotBetween(Integer value1, Integer value2) {
            addCriterion("columnid not between", value1, value2, "columnid");
            return (Criteria) this;
        }

        public Criteria andAuthoridIsNull() {
            addCriterion("authorid is null");
            return (Criteria) this;
        }

        public Criteria andAuthoridIsNotNull() {
            addCriterion("authorid is not null");
            return (Criteria) this;
        }

        public Criteria andAuthoridEqualTo(Integer value) {
            addCriterion("authorid =", value, "authorid");
            return (Criteria) this;
        }

        public Criteria andAuthoridNotEqualTo(Integer value) {
            addCriterion("authorid <>", value, "authorid");
            return (Criteria) this;
        }

        public Criteria andAuthoridGreaterThan(Integer value) {
            addCriterion("authorid >", value, "authorid");
            return (Criteria) this;
        }

        public Criteria andAuthoridGreaterThanOrEqualTo(Integer value) {
            addCriterion("authorid >=", value, "authorid");
            return (Criteria) this;
        }

        public Criteria andAuthoridLessThan(Integer value) {
            addCriterion("authorid <", value, "authorid");
            return (Criteria) this;
        }

        public Criteria andAuthoridLessThanOrEqualTo(Integer value) {
            addCriterion("authorid <=", value, "authorid");
            return (Criteria) this;
        }

        public Criteria andAuthoridIn(List<Integer> values) {
            addCriterion("authorid in", values, "authorid");
            return (Criteria) this;
        }

        public Criteria andAuthoridNotIn(List<Integer> values) {
            addCriterion("authorid not in", values, "authorid");
            return (Criteria) this;
        }

        public Criteria andAuthoridBetween(Integer value1, Integer value2) {
            addCriterion("authorid between", value1, value2, "authorid");
            return (Criteria) this;
        }

        public Criteria andAuthoridNotBetween(Integer value1, Integer value2) {
            addCriterion("authorid not between", value1, value2, "authorid");
            return (Criteria) this;
        }

        public Criteria andAttentiontimeIsNull() {
            addCriterion("attentiontime is null");
            return (Criteria) this;
        }

        public Criteria andAttentiontimeIsNotNull() {
            addCriterion("attentiontime is not null");
            return (Criteria) this;
        }

        public Criteria andAttentiontimeEqualTo(Date value) {
            addCriterion("attentiontime =", value, "attentiontime");
            return (Criteria) this;
        }

        public Criteria andAttentiontimeNotEqualTo(Date value) {
            addCriterion("attentiontime <>", value, "attentiontime");
            return (Criteria) this;
        }

        public Criteria andAttentiontimeGreaterThan(Date value) {
            addCriterion("attentiontime >", value, "attentiontime");
            return (Criteria) this;
        }

        public Criteria andAttentiontimeGreaterThanOrEqualTo(Date value) {
            addCriterion("attentiontime >=", value, "attentiontime");
            return (Criteria) this;
        }

        public Criteria andAttentiontimeLessThan(Date value) {
            addCriterion("attentiontime <", value, "attentiontime");
            return (Criteria) this;
        }

        public Criteria andAttentiontimeLessThanOrEqualTo(Date value) {
            addCriterion("attentiontime <=", value, "attentiontime");
            return (Criteria) this;
        }

        public Criteria andAttentiontimeIn(List<Date> values) {
            addCriterion("attentiontime in", values, "attentiontime");
            return (Criteria) this;
        }

        public Criteria andAttentiontimeNotIn(List<Date> values) {
            addCriterion("attentiontime not in", values, "attentiontime");
            return (Criteria) this;
        }

        public Criteria andAttentiontimeBetween(Date value1, Date value2) {
            addCriterion("attentiontime between", value1, value2, "attentiontime");
            return (Criteria) this;
        }

        public Criteria andAttentiontimeNotBetween(Date value1, Date value2) {
            addCriterion("attentiontime not between", value1, value2, "attentiontime");
            return (Criteria) this;
        }

        public Criteria andColumnIsNull() {
            addCriterion("column is null");
            return (Criteria) this;
        }

        public Criteria andColumnIsNotNull() {
            addCriterion("column is not null");
            return (Criteria) this;
        }

        public Criteria andColumnEqualTo(String value) {
            addCriterion("column =", value, "column");
            return (Criteria) this;
        }

        public Criteria andColumnNotEqualTo(String value) {
            addCriterion("column <>", value, "column");
            return (Criteria) this;
        }

        public Criteria andColumnGreaterThan(String value) {
            addCriterion("column >", value, "column");
            return (Criteria) this;
        }

        public Criteria andColumnGreaterThanOrEqualTo(String value) {
            addCriterion("column >=", value, "column");
            return (Criteria) this;
        }

        public Criteria andColumnLessThan(String value) {
            addCriterion("column <", value, "column");
            return (Criteria) this;
        }

        public Criteria andColumnLessThanOrEqualTo(String value) {
            addCriterion("column <=", value, "column");
            return (Criteria) this;
        }

        public Criteria andColumnLike(String value) {
            addCriterion("column like", value, "column");
            return (Criteria) this;
        }

        public Criteria andColumnNotLike(String value) {
            addCriterion("column not like", value, "column");
            return (Criteria) this;
        }

        public Criteria andColumnIn(List<String> values) {
            addCriterion("column in", values, "column");
            return (Criteria) this;
        }

        public Criteria andColumnNotIn(List<String> values) {
            addCriterion("column not in", values, "column");
            return (Criteria) this;
        }

        public Criteria andColumnBetween(String value1, String value2) {
            addCriterion("column between", value1, value2, "column");
            return (Criteria) this;
        }

        public Criteria andColumnNotBetween(String value1, String value2) {
            addCriterion("column not between", value1, value2, "column");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}