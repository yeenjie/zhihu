package com.phoenix.zhihu.entity;

public class ReplyList {
    private int criticsid;
    private int becriticsid;
    private String content;

    public int getCriticsid() {
        return criticsid;
    }

    public void setCriticsid(int criticsid) {
        this.criticsid = criticsid;
    }

    public int getBecriticsid() {
        return becriticsid;
    }

    public void setBecriticsid(int becriticsid) {
        this.becriticsid = becriticsid;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
