package com.phoenix.zhihu.entity;

import java.util.Date;

public class Invite {
    private Integer id;

    private Integer inviterid;

    private Integer inviteeid;

    private Integer answerid;

    private Date invitetime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getInviterid() {
        return inviterid;
    }

    public void setInviterid(Integer inviterid) {
        this.inviterid = inviterid;
    }

    public Integer getInviteeid() {
        return inviteeid;
    }

    public void setInviteeid(Integer inviteeid) {
        this.inviteeid = inviteeid;
    }

    public Integer getAnswerid() {
        return answerid;
    }

    public void setAnswerid(Integer answerid) {
        this.answerid = answerid;
    }

    public Date getInvitetime() {
        return invitetime;
    }

    public void setInvitetime(Date invitetime) {
        this.invitetime = invitetime;
    }
}