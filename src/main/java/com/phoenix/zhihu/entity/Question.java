package com.phoenix.zhihu.entity;

import java.util.Date;

public class Question {
    private Integer id;

    private Integer userid;

    private String questionname;

    private String questiondescribe;

    private String image;

    private Integer answertimes;

    private Integer skimtimes;

    private Date createtime;

    private Integer anonymous;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserid() {
        return userid;
    }

    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    public String getQuestionname() {
        return questionname;
    }

    public void setQuestionname(String questionname) {
        this.questionname = questionname == null ? null : questionname.trim();
    }

    public String getQuestiondescribe() {
        return questiondescribe;
    }

    public void setQuestiondescribe(String questiondescribe) {
        this.questiondescribe = questiondescribe == null ? null : questiondescribe.trim();
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image == null ? null : image.trim();
    }

    public Integer getAnswertimes() {
        return answertimes;
    }

    public void setAnswertimes(Integer answertimes) {
        this.answertimes = answertimes;
    }

    public Integer getSkimtimes() {
        return skimtimes;
    }

    public void setSkimtimes(Integer skimtimes) {
        this.skimtimes = skimtimes;
    }

    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    public Integer getAnonymous() {
        return anonymous;
    }

    public void setAnonymous(Integer anonymous) {
        this.anonymous = anonymous;
    }

    @Override
    public String toString() {
        return "Question{" +
                "id=" + id +
                ", userid=" + userid +
                ", questionname='" + questionname + '\'' +
                ", questiondescribe='" + questiondescribe + '\'' +
                ", image='" + image + '\'' +
                ", answertimes=" + answertimes +
                ", skimtimes=" + skimtimes +
                ", createtime=" + createtime +
                ", anonymous=" + anonymous +
                '}';
    }
}