package com.phoenix.zhihu.entity;


public class CommentList {
    //评论内容
    private String content;

    //该条评论的id,条件为becriticsid为空
    private int bearticleid;

    private String username;

    //评论者的id
    private int criticsid;

    private int answerid;

    //该评论人的头像
    private String dpPath;

    public String getDpPath() {
        return dpPath;
    }

    public void setDpPath(String dpPath) {
        this.dpPath = dpPath;
    }

    public int getAnswerid() {
        return answerid;
    }

    public void setAnswerid(int answerid) {
        this.answerid = answerid;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getBearticleid() {
        return bearticleid;
    }

    public void setBearticleid(int bearticleid) {
        this.bearticleid = bearticleid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getCriticsid() {
        return criticsid;
    }

    public void setCriticsid(int criticsid) {
        this.criticsid = criticsid;
    }
}
