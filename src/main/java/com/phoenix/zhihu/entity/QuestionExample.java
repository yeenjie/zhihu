package com.phoenix.zhihu.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class QuestionExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public QuestionExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andUseridIsNull() {
            addCriterion("userid is null");
            return (Criteria) this;
        }

        public Criteria andUseridIsNotNull() {
            addCriterion("userid is not null");
            return (Criteria) this;
        }

        public Criteria andUseridEqualTo(Integer value) {
            addCriterion("userid =", value, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridNotEqualTo(Integer value) {
            addCriterion("userid <>", value, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridGreaterThan(Integer value) {
            addCriterion("userid >", value, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridGreaterThanOrEqualTo(Integer value) {
            addCriterion("userid >=", value, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridLessThan(Integer value) {
            addCriterion("userid <", value, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridLessThanOrEqualTo(Integer value) {
            addCriterion("userid <=", value, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridIn(List<Integer> values) {
            addCriterion("userid in", values, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridNotIn(List<Integer> values) {
            addCriterion("userid not in", values, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridBetween(Integer value1, Integer value2) {
            addCriterion("userid between", value1, value2, "userid");
            return (Criteria) this;
        }

        public Criteria andUseridNotBetween(Integer value1, Integer value2) {
            addCriterion("userid not between", value1, value2, "userid");
            return (Criteria) this;
        }

        public Criteria andQuestionnameIsNull() {
            addCriterion("questionname is null");
            return (Criteria) this;
        }

        public Criteria andQuestionnameIsNotNull() {
            addCriterion("questionname is not null");
            return (Criteria) this;
        }

        public Criteria andQuestionnameEqualTo(String value) {
            addCriterion("questionname =", value, "questionname");
            return (Criteria) this;
        }

        public Criteria andQuestionnameNotEqualTo(String value) {
            addCriterion("questionname <>", value, "questionname");
            return (Criteria) this;
        }

        public Criteria andQuestionnameGreaterThan(String value) {
            addCriterion("questionname >", value, "questionname");
            return (Criteria) this;
        }

        public Criteria andQuestionnameGreaterThanOrEqualTo(String value) {
            addCriterion("questionname >=", value, "questionname");
            return (Criteria) this;
        }

        public Criteria andQuestionnameLessThan(String value) {
            addCriterion("questionname <", value, "questionname");
            return (Criteria) this;
        }

        public Criteria andQuestionnameLessThanOrEqualTo(String value) {
            addCriterion("questionname <=", value, "questionname");
            return (Criteria) this;
        }

        public Criteria andQuestionnameLike(String value) {
            addCriterion("questionname like", value, "questionname");
            return (Criteria) this;
        }

        public Criteria andQuestionnameNotLike(String value) {
            addCriterion("questionname not like", value, "questionname");
            return (Criteria) this;
        }

        public Criteria andQuestionnameIn(List<String> values) {
            addCriterion("questionname in", values, "questionname");
            return (Criteria) this;
        }

        public Criteria andQuestionnameNotIn(List<String> values) {
            addCriterion("questionname not in", values, "questionname");
            return (Criteria) this;
        }

        public Criteria andQuestionnameBetween(String value1, String value2) {
            addCriterion("questionname between", value1, value2, "questionname");
            return (Criteria) this;
        }

        public Criteria andQuestionnameNotBetween(String value1, String value2) {
            addCriterion("questionname not between", value1, value2, "questionname");
            return (Criteria) this;
        }

        public Criteria andQuestiondescribeIsNull() {
            addCriterion("questiondescribe is null");
            return (Criteria) this;
        }

        public Criteria andQuestiondescribeIsNotNull() {
            addCriterion("questiondescribe is not null");
            return (Criteria) this;
        }

        public Criteria andQuestiondescribeEqualTo(String value) {
            addCriterion("questiondescribe =", value, "questiondescribe");
            return (Criteria) this;
        }

        public Criteria andQuestiondescribeNotEqualTo(String value) {
            addCriterion("questiondescribe <>", value, "questiondescribe");
            return (Criteria) this;
        }

        public Criteria andQuestiondescribeGreaterThan(String value) {
            addCriterion("questiondescribe >", value, "questiondescribe");
            return (Criteria) this;
        }

        public Criteria andQuestiondescribeGreaterThanOrEqualTo(String value) {
            addCriterion("questiondescribe >=", value, "questiondescribe");
            return (Criteria) this;
        }

        public Criteria andQuestiondescribeLessThan(String value) {
            addCriterion("questiondescribe <", value, "questiondescribe");
            return (Criteria) this;
        }

        public Criteria andQuestiondescribeLessThanOrEqualTo(String value) {
            addCriterion("questiondescribe <=", value, "questiondescribe");
            return (Criteria) this;
        }

        public Criteria andQuestiondescribeLike(String value) {
            addCriterion("questiondescribe like", value, "questiondescribe");
            return (Criteria) this;
        }

        public Criteria andQuestiondescribeNotLike(String value) {
            addCriterion("questiondescribe not like", value, "questiondescribe");
            return (Criteria) this;
        }

        public Criteria andQuestiondescribeIn(List<String> values) {
            addCriterion("questiondescribe in", values, "questiondescribe");
            return (Criteria) this;
        }

        public Criteria andQuestiondescribeNotIn(List<String> values) {
            addCriterion("questiondescribe not in", values, "questiondescribe");
            return (Criteria) this;
        }

        public Criteria andQuestiondescribeBetween(String value1, String value2) {
            addCriterion("questiondescribe between", value1, value2, "questiondescribe");
            return (Criteria) this;
        }

        public Criteria andQuestiondescribeNotBetween(String value1, String value2) {
            addCriterion("questiondescribe not between", value1, value2, "questiondescribe");
            return (Criteria) this;
        }

        public Criteria andImageIsNull() {
            addCriterion("image is null");
            return (Criteria) this;
        }

        public Criteria andImageIsNotNull() {
            addCriterion("image is not null");
            return (Criteria) this;
        }

        public Criteria andImageEqualTo(String value) {
            addCriterion("image =", value, "image");
            return (Criteria) this;
        }

        public Criteria andImageNotEqualTo(String value) {
            addCriterion("image <>", value, "image");
            return (Criteria) this;
        }

        public Criteria andImageGreaterThan(String value) {
            addCriterion("image >", value, "image");
            return (Criteria) this;
        }

        public Criteria andImageGreaterThanOrEqualTo(String value) {
            addCriterion("image >=", value, "image");
            return (Criteria) this;
        }

        public Criteria andImageLessThan(String value) {
            addCriterion("image <", value, "image");
            return (Criteria) this;
        }

        public Criteria andImageLessThanOrEqualTo(String value) {
            addCriterion("image <=", value, "image");
            return (Criteria) this;
        }

        public Criteria andImageLike(String value) {
            addCriterion("image like", value, "image");
            return (Criteria) this;
        }

        public Criteria andImageNotLike(String value) {
            addCriterion("image not like", value, "image");
            return (Criteria) this;
        }

        public Criteria andImageIn(List<String> values) {
            addCriterion("image in", values, "image");
            return (Criteria) this;
        }

        public Criteria andImageNotIn(List<String> values) {
            addCriterion("image not in", values, "image");
            return (Criteria) this;
        }

        public Criteria andImageBetween(String value1, String value2) {
            addCriterion("image between", value1, value2, "image");
            return (Criteria) this;
        }

        public Criteria andImageNotBetween(String value1, String value2) {
            addCriterion("image not between", value1, value2, "image");
            return (Criteria) this;
        }

        public Criteria andAnswertimesIsNull() {
            addCriterion("answertimes is null");
            return (Criteria) this;
        }

        public Criteria andAnswertimesIsNotNull() {
            addCriterion("answertimes is not null");
            return (Criteria) this;
        }

        public Criteria andAnswertimesEqualTo(Integer value) {
            addCriterion("answertimes =", value, "answertimes");
            return (Criteria) this;
        }

        public Criteria andAnswertimesNotEqualTo(Integer value) {
            addCriterion("answertimes <>", value, "answertimes");
            return (Criteria) this;
        }

        public Criteria andAnswertimesGreaterThan(Integer value) {
            addCriterion("answertimes >", value, "answertimes");
            return (Criteria) this;
        }

        public Criteria andAnswertimesGreaterThanOrEqualTo(Integer value) {
            addCriterion("answertimes >=", value, "answertimes");
            return (Criteria) this;
        }

        public Criteria andAnswertimesLessThan(Integer value) {
            addCriterion("answertimes <", value, "answertimes");
            return (Criteria) this;
        }

        public Criteria andAnswertimesLessThanOrEqualTo(Integer value) {
            addCriterion("answertimes <=", value, "answertimes");
            return (Criteria) this;
        }

        public Criteria andAnswertimesIn(List<Integer> values) {
            addCriterion("answertimes in", values, "answertimes");
            return (Criteria) this;
        }

        public Criteria andAnswertimesNotIn(List<Integer> values) {
            addCriterion("answertimes not in", values, "answertimes");
            return (Criteria) this;
        }

        public Criteria andAnswertimesBetween(Integer value1, Integer value2) {
            addCriterion("answertimes between", value1, value2, "answertimes");
            return (Criteria) this;
        }

        public Criteria andAnswertimesNotBetween(Integer value1, Integer value2) {
            addCriterion("answertimes not between", value1, value2, "answertimes");
            return (Criteria) this;
        }

        public Criteria andSkimtimesIsNull() {
            addCriterion("skimtimes is null");
            return (Criteria) this;
        }

        public Criteria andSkimtimesIsNotNull() {
            addCriterion("skimtimes is not null");
            return (Criteria) this;
        }

        public Criteria andSkimtimesEqualTo(Integer value) {
            addCriterion("skimtimes =", value, "skimtimes");
            return (Criteria) this;
        }

        public Criteria andSkimtimesNotEqualTo(Integer value) {
            addCriterion("skimtimes <>", value, "skimtimes");
            return (Criteria) this;
        }

        public Criteria andSkimtimesGreaterThan(Integer value) {
            addCriterion("skimtimes >", value, "skimtimes");
            return (Criteria) this;
        }

        public Criteria andSkimtimesGreaterThanOrEqualTo(Integer value) {
            addCriterion("skimtimes >=", value, "skimtimes");
            return (Criteria) this;
        }

        public Criteria andSkimtimesLessThan(Integer value) {
            addCriterion("skimtimes <", value, "skimtimes");
            return (Criteria) this;
        }

        public Criteria andSkimtimesLessThanOrEqualTo(Integer value) {
            addCriterion("skimtimes <=", value, "skimtimes");
            return (Criteria) this;
        }

        public Criteria andSkimtimesIn(List<Integer> values) {
            addCriterion("skimtimes in", values, "skimtimes");
            return (Criteria) this;
        }

        public Criteria andSkimtimesNotIn(List<Integer> values) {
            addCriterion("skimtimes not in", values, "skimtimes");
            return (Criteria) this;
        }

        public Criteria andSkimtimesBetween(Integer value1, Integer value2) {
            addCriterion("skimtimes between", value1, value2, "skimtimes");
            return (Criteria) this;
        }

        public Criteria andSkimtimesNotBetween(Integer value1, Integer value2) {
            addCriterion("skimtimes not between", value1, value2, "skimtimes");
            return (Criteria) this;
        }

        public Criteria andCreatetimeIsNull() {
            addCriterion("createtime is null");
            return (Criteria) this;
        }

        public Criteria andCreatetimeIsNotNull() {
            addCriterion("createtime is not null");
            return (Criteria) this;
        }

        public Criteria andCreatetimeEqualTo(Date value) {
            addCriterion("createtime =", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeNotEqualTo(Date value) {
            addCriterion("createtime <>", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeGreaterThan(Date value) {
            addCriterion("createtime >", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeGreaterThanOrEqualTo(Date value) {
            addCriterion("createtime >=", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeLessThan(Date value) {
            addCriterion("createtime <", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeLessThanOrEqualTo(Date value) {
            addCriterion("createtime <=", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeIn(List<Date> values) {
            addCriterion("createtime in", values, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeNotIn(List<Date> values) {
            addCriterion("createtime not in", values, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeBetween(Date value1, Date value2) {
            addCriterion("createtime between", value1, value2, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeNotBetween(Date value1, Date value2) {
            addCriterion("createtime not between", value1, value2, "createtime");
            return (Criteria) this;
        }

        public Criteria andAnonymousIsNull() {
            addCriterion("anonymous is null");
            return (Criteria) this;
        }

        public Criteria andAnonymousIsNotNull() {
            addCriterion("anonymous is not null");
            return (Criteria) this;
        }

        public Criteria andAnonymousEqualTo(Integer value) {
            addCriterion("anonymous =", value, "anonymous");
            return (Criteria) this;
        }

        public Criteria andAnonymousNotEqualTo(Integer value) {
            addCriterion("anonymous <>", value, "anonymous");
            return (Criteria) this;
        }

        public Criteria andAnonymousGreaterThan(Integer value) {
            addCriterion("anonymous >", value, "anonymous");
            return (Criteria) this;
        }

        public Criteria andAnonymousGreaterThanOrEqualTo(Integer value) {
            addCriterion("anonymous >=", value, "anonymous");
            return (Criteria) this;
        }

        public Criteria andAnonymousLessThan(Integer value) {
            addCriterion("anonymous <", value, "anonymous");
            return (Criteria) this;
        }

        public Criteria andAnonymousLessThanOrEqualTo(Integer value) {
            addCriterion("anonymous <=", value, "anonymous");
            return (Criteria) this;
        }

        public Criteria andAnonymousIn(List<Integer> values) {
            addCriterion("anonymous in", values, "anonymous");
            return (Criteria) this;
        }

        public Criteria andAnonymousNotIn(List<Integer> values) {
            addCriterion("anonymous not in", values, "anonymous");
            return (Criteria) this;
        }

        public Criteria andAnonymousBetween(Integer value1, Integer value2) {
            addCriterion("anonymous between", value1, value2, "anonymous");
            return (Criteria) this;
        }

        public Criteria andAnonymousNotBetween(Integer value1, Integer value2) {
            addCriterion("anonymous not between", value1, value2, "anonymous");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}