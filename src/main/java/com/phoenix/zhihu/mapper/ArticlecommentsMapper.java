package com.phoenix.zhihu.mapper;

import com.phoenix.zhihu.entity.Articlecomments;
import com.phoenix.zhihu.entity.ArticlecommentsExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface ArticlecommentsMapper {
    int countByExample(ArticlecommentsExample example);

    int deleteByExample(ArticlecommentsExample example);

    int insert(Articlecomments record);

    int insertSelective(Articlecomments record);

    List<Articlecomments> selectByExample(ArticlecommentsExample example);

    int updateByExampleSelective(@Param("record") Articlecomments record, @Param("example") ArticlecommentsExample example);

    int updateByExample(@Param("record") Articlecomments record, @Param("example") ArticlecommentsExample example);
}