package com.phoenix.zhihu.mapper;

import com.phoenix.zhihu.entity.Collectionarticle;
import com.phoenix.zhihu.entity.CollectionarticleExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CollectionarticleMapper {
    int countByExample(CollectionarticleExample example);

    int deleteByExample(CollectionarticleExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Collectionarticle record);

    int insertSelective(Collectionarticle record);

    List<Collectionarticle> selectByExample(CollectionarticleExample example);

    Collectionarticle selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Collectionarticle record, @Param("example") CollectionarticleExample example);

    int updateByExample(@Param("record") Collectionarticle record, @Param("example") CollectionarticleExample example);

    int updateByPrimaryKeySelective(Collectionarticle record);

    int updateByPrimaryKey(Collectionarticle record);
}