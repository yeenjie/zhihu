package com.phoenix.zhihu.mapper;

import com.phoenix.zhihu.entity.Arctic;
import com.phoenix.zhihu.entity.ArcticExample;
import java.util.List;

import com.phoenix.zhihu.entity.ArticleDetail;
import com.phoenix.zhihu.entity.ArticleList;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ArcticMapper {
    int countByExample(ArcticExample example);

    int deleteByExample(ArcticExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Arctic record);

    int insertSelective(Arctic record);

    List<Arctic> selectByExampleWithBLOBs(ArcticExample example);

    List<Arctic> selectByExample(ArcticExample example);

    Arctic selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Arctic record, @Param("example") ArcticExample example);

    int updateByExampleWithBLOBs(@Param("record") Arctic record, @Param("example") ArcticExample example);

    int updateByExample(@Param("record") Arctic record, @Param("example") ArcticExample example);

    int updateByPrimaryKeySelective(Arctic record);

    int updateByPrimaryKeyWithBLOBs(Arctic record);

    int updateByPrimaryKey(Arctic record);

    List<ArticleList> getArticles();

    void addAprove(String articleid);

    void cancleAprove(String articleid);

    ArticleList getArticleDetails(int articleid);
}