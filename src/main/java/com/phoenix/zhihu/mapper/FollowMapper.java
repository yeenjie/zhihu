package com.phoenix.zhihu.mapper;

import com.phoenix.zhihu.entity.Follow;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FollowMapper {

    public List<Follow> getFollowId(@Param("uid") long uid);

    public void follow(@Param("uid") long uid, @Param("fid") long fid);

    public void removeFollow(@Param("uid") long uid, @Param("fid") long fid);
}
