package com.phoenix.zhihu.mapper;

import com.phoenix.zhihu.entity.Collection;
import com.phoenix.zhihu.entity.Collectionnew;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CollectionnewMapper {
    List<Collectionnew> getCollection(@Param("userid") long userid);

    void removeCollection(@Param("userid") long userid,@Param("answerid") long answerid);

    void insertCollection(@Param("userid") long userid,@Param("answerid") long answerid);
}
