package com.phoenix.zhihu.mapper;

import com.phoenix.zhihu.entity.Answercomment;
import com.phoenix.zhihu.entity.AnswercommentExample;
import java.util.List;
import java.util.Map;

import com.phoenix.zhihu.entity.CommentList;
import com.phoenix.zhihu.entity.ReplyList;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;


@Repository
public interface AnswercommentMapper {
    int countByExample(AnswercommentExample example);

    int deleteByExample(AnswercommentExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Answercomment record);

    int insertSelective(Answercomment record);

    List<Answercomment> selectByExample(AnswercommentExample example);

    Answercomment selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Answercomment record, @Param("example") AnswercommentExample example);

    int updateByExample(@Param("record") Answercomment record, @Param("example") AnswercommentExample example);

    int updateByPrimaryKeySelective(Answercomment record);

    int updateByPrimaryKey(Answercomment record);

    List<CommentList> selectAllComments(int answerid);

    List<ReplyList> getAllReplies(Map<String,Object> map);
}