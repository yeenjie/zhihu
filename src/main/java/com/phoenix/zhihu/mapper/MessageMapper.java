package com.phoenix.zhihu.mapper;

import com.phoenix.zhihu.entity.Message;
import com.phoenix.zhihu.entity.MessageExample;
import java.util.List;
import java.util.Map;

import com.phoenix.zhihu.entity.resultObject.Message_User;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;


@Repository
public interface MessageMapper {
    int countByExample(MessageExample example);

    int deleteByExample(MessageExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Message record);

    int insertSelective(Message record);

    List<Message> selectByExample(MessageExample example);

    Message selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Message record, @Param("example") MessageExample example);

    int updateByExample(@Param("record") Message record, @Param("example") MessageExample example);

    int updateByPrimaryKeySelective(Message record);

    int updateByPrimaryKey(Message record);

    List<Message_User> getByReceiverid(@Param("receiverid") long receiverid);

    List<Message_User> getReturnMessage(@Param("senderid") long senderid,@Param("receiverid") long receiverid);

    List<Message_User> getNewMessage(@Param("receiverid") long receiverid);

}