package com.phoenix.zhihu.mapper;

import com.phoenix.zhihu.entity.User;
import com.phoenix.zhihu.entity.UserExample;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UserMapper {
    int countByExample(UserExample example);

    int deleteByExample(UserExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(User record);

    int insertSelective(User record);

    List<User> selectByExample(UserExample example);

    User selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") User record, @Param("example") UserExample example);

    int updateByExample(@Param("record") User record, @Param("example") UserExample example);

    int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);


    String getPasswordByName(@Param("username") String username);

    String getUsernameById(@Param("id") long id);

    Long getIdByUsername(@Param("username") String username);

    User checkUsername(@Param("username") String username);

    void insertRegister(@Param("username") String username, @Param("password") String password);
    String getDpById(@Param("id") long id);
}