package com.phoenix.zhihu.mapper;

import com.phoenix.zhihu.entity.Questiontopic;
import com.phoenix.zhihu.entity.QuestiontopicExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface QuestiontopicMapper {
    int countByExample(QuestiontopicExample example);

    int deleteByExample(QuestiontopicExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Questiontopic record);

    int insertSelective(Questiontopic record);

    List<Questiontopic> selectByExample(QuestiontopicExample example);

    Questiontopic selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Questiontopic record, @Param("example") QuestiontopicExample example);

    int updateByExample(@Param("record") Questiontopic record, @Param("example") QuestiontopicExample example);

    int updateByPrimaryKeySelective(Questiontopic record);

    int updateByPrimaryKey(Questiontopic record);
}