package com.phoenix.zhihu.mapper;

import com.phoenix.zhihu.entity.Attentioncolumn;
import com.phoenix.zhihu.entity.AttentioncolumnExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface AttentioncolumnMapper {
    int countByExample(AttentioncolumnExample example);

    int deleteByExample(AttentioncolumnExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Attentioncolumn record);

    int insertSelective(Attentioncolumn record);

    List<Attentioncolumn> selectByExample(AttentioncolumnExample example);

    Attentioncolumn selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Attentioncolumn record, @Param("example") AttentioncolumnExample example);

    int updateByExample(@Param("record") Attentioncolumn record, @Param("example") AttentioncolumnExample example);

    int updateByPrimaryKeySelective(Attentioncolumn record);

    int updateByPrimaryKey(Attentioncolumn record);
}