package com.phoenix.zhihu.mapper;

import com.phoenix.zhihu.entity.Answer;
import com.phoenix.zhihu.entity.AnswerExample;
import java.util.List;

import com.phoenix.zhihu.entity.AnswerList;
import com.phoenix.zhihu.entity.resultObject.User_Collection;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface AnswerMapper {
    int countByExample(AnswerExample example);

    int deleteByExample(AnswerExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Answer record);

    int insertSelective(Answer record);

    List<Answer> selectByExampleWithBLOBs(AnswerExample example);

    List<Answer> selectByExample(AnswerExample example);

    Answer selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Answer record, @Param("example") AnswerExample example);

    int updateByExampleWithBLOBs(@Param("record") Answer record, @Param("example") AnswerExample example);

    int updateByExample(@Param("record") Answer record, @Param("example") AnswerExample example);

    int updateByPrimaryKeySelective(Answer record);

    int updateByPrimaryKeyWithBLOBs(Answer record);

    int updateByPrimaryKey(Answer record);

    List<AnswerList> getDiffAnswers();

    User_Collection getById(@Param("id") long id);

    void addAprove(@Param("answerid") long answerid);

    void removeAprove(@Param("answerid") long answerid);

    List<AnswerList> getAnswers(int questionid);

    void increaseCommentTimes(int answerid);

    List<AnswerList> getMyAnswers(long userid);
}