package com.phoenix.zhihu.mapper;

import com.phoenix.zhihu.entity.User;
import com.phoenix.zhihu.entity.resultObject.Article_Admin;
import com.phoenix.zhihu.entity.resultObject.Question_Admin;
import com.phoenix.zhihu.entity.resultObject.User_Admin;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.List;

@Repository
public interface AdminMapper {

    @Select("select * from user")
    public List<User_Admin> getUser();

    @Update("update user set role = '1' where id = #{id}")
    public void setUserStatus1(@Param("id") long id);

    @Update("update user set role = '0' where id = #{id}")
    public void setUserStatus0(@Param("id") long id);

    @Select("select * from question")
    public List<Question_Admin> getQuestion();

    @Select("select username from user where id = #{id}")
    public String getUsernameById(@Param("id") long id);

    @Select("select * from arctic")
    public List<Article_Admin> getArticle();

    @Delete("delete from question where id = #{id}")
    public void deleteQuestion(@Param("id") long id);
}
