package com.phoenix.zhihu.mapper;

import com.phoenix.zhihu.entity.Useranswer;
import com.phoenix.zhihu.entity.UseranswerExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface UseranswerMapper {
    int countByExample(UseranswerExample example);

    int deleteByExample(UseranswerExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Useranswer record);

    int insertSelective(Useranswer record);

    List<Useranswer> selectByExample(UseranswerExample example);

    Useranswer selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Useranswer record, @Param("example") UseranswerExample example);

    int updateByExample(@Param("record") Useranswer record, @Param("example") UseranswerExample example);

    int updateByPrimaryKeySelective(Useranswer record);

    int updateByPrimaryKey(Useranswer record);
}