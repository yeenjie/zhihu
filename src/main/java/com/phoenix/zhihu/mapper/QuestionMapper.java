package com.phoenix.zhihu.mapper;

import com.phoenix.zhihu.entity.Question;
import com.phoenix.zhihu.entity.QuestionExample;
import java.util.List;

import com.phoenix.zhihu.entity.QuestionList;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface QuestionMapper {
    int countByExample(QuestionExample example);

    int deleteByExample(QuestionExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Question record);

    int insertSelective(Question record);

    List<Question> selectByExample(QuestionExample example);

    Question selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Question record, @Param("example") QuestionExample example);

    int updateByExample(@Param("record") Question record, @Param("example") QuestionExample example);

    int updateByPrimaryKeySelective(Question record);

    int updateByPrimaryKey(Question record);

    /**
     * 查询所有问题
     * */
    List<Question> selectAll();

    /**
     * 获取该问题的所有回答
     * */
    List<QuestionList> getQuestionList(int questionid);

    String getQuestionNameById(int questionid);

    String getQuestionDescribeById(int questionid);

    String getQuestionSkimTimes(int questionid);

    void increaseSkimTimes(int questionid);

    List<Question> getHotData();

    List<QuestionList> queryQuestion(String questionname);

    List<QuestionList> getQuestionByUserId(int userid);

    String getQuestionnameById (@Param("id") long id);
}