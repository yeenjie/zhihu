package com.phoenix.zhihu.mapper;

import com.phoenix.zhihu.entity.Followtopic;
import com.phoenix.zhihu.entity.FollowtopicExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface FollowtopicMapper {
    int countByExample(FollowtopicExample example);

    int deleteByExample(FollowtopicExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Followtopic record);

    int insertSelective(Followtopic record);

    List<Followtopic> selectByExample(FollowtopicExample example);

    Followtopic selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Followtopic record, @Param("example") FollowtopicExample example);

    int updateByExample(@Param("record") Followtopic record, @Param("example") FollowtopicExample example);

    int updateByPrimaryKeySelective(Followtopic record);

    int updateByPrimaryKey(Followtopic record);
}