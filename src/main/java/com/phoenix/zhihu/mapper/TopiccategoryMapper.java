package com.phoenix.zhihu.mapper;

import com.phoenix.zhihu.entity.Topiccategory;
import com.phoenix.zhihu.entity.TopiccategoryExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface TopiccategoryMapper {
    int countByExample(TopiccategoryExample example);

    int deleteByExample(TopiccategoryExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Topiccategory record);

    int insertSelective(Topiccategory record);

    List<Topiccategory> selectByExample(TopiccategoryExample example);

    Topiccategory selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Topiccategory record, @Param("example") TopiccategoryExample example);

    int updateByExample(@Param("record") Topiccategory record, @Param("example") TopiccategoryExample example);

    int updateByPrimaryKeySelective(Topiccategory record);

    int updateByPrimaryKey(Topiccategory record);
}