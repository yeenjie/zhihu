package com.phoenix.zhihu.service;

import com.phoenix.zhihu.entity.Collectionanswer;

import java.util.List;

public interface CollectionService {
    List<Collectionanswer> getCollectionListByUserId(int userid);
}
