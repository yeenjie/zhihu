package com.phoenix.zhihu.service;

import com.phoenix.zhihu.entity.Question;
import com.phoenix.zhihu.entity.QuestionList;

import java.util.List;

public interface QuestionService {
    public void addQuestion(Question question);
    public void deleteQuestionById(int id);
    public void updateQuestion(Question question);
    public List<QuestionList> queryQuestion(String questionname);
    public List<QuestionList> getQuestionByUserId(int userid);
}
