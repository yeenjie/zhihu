package com.phoenix.zhihu.service;

import com.phoenix.zhihu.entity.User;
import com.sun.org.apache.xpath.internal.operations.Bool;

public interface LoginService {

    public String Login(User user);

    public boolean register(User user);

    public long getIdByUsername(String username);

    public Boolean checkUsername(String username);

    public String getUsernameById(long id);


}
