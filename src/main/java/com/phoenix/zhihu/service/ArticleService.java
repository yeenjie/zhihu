package com.phoenix.zhihu.service;

import com.phoenix.zhihu.entity.Arctic;
import com.phoenix.zhihu.entity.ArticleDetail;
import com.phoenix.zhihu.entity.ArticleList;
import com.phoenix.zhihu.mapper.ArcticMapper;

public interface ArticleService {
    public void addArticle(Arctic arctic);

    void addAproveArticle(String kind, String articleid);

    ArticleList getArticleDetails(int articleid);
}
