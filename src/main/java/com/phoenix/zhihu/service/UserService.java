package com.phoenix.zhihu.service;

import javax.servlet.http.HttpServletRequest;

public interface UserService {
    /**
     * 添加用户头像地址
     * @param userid 用户的id
     * @param url 添加的地址
     */
    public void addUserImage(int userid,String url);

    /**
     * 获得当前登陆用户id
     * @return
     */
    public int getNowUserId(HttpServletRequest request);
}
