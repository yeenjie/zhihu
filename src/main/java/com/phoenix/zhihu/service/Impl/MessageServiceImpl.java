package com.phoenix.zhihu.service.Impl;

import com.phoenix.zhihu.entity.Message;
import com.phoenix.zhihu.entity.resultObject.Message_User;
import com.phoenix.zhihu.mapper.MessageMapper;
import com.phoenix.zhihu.mapper.UserMapper;
import com.phoenix.zhihu.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sun.util.resources.es.LocaleNames_es_US;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Service
public class MessageServiceImpl implements MessageService {

    @Autowired
    MessageMapper messageMapper;

    @Autowired
    UserMapper userMapper;

    @Override
    public List<Message_User> getMessage(long receiverid) {
        try {
            List<Message_User> list = messageMapper.getByReceiverid(receiverid);

            for (int i = 0; i < list.size(); i++) {

                for (int j = i + 1; j < list.size(); ) {

                    if (j == list.size()) {
                        break;
                    }
                    int tempi = list.get(i).getSenderid();
                    int tempj = list.get(j).getSenderid();
//                System.out.println("内层循环层:"+tempj+"和"+tempi+"对比");

                    if (tempi == tempj) {

                        list.remove(j);
                        break;
                    } else {
                        j++;
                    }

                }
            }


            return list;
        } catch (Exception e) {
            return null;
        }

    }

    @Override
    public List<Message_User> getNewMessage(long receiverid) {
        List<Message_User> list = messageMapper.getNewMessage(receiverid);


//        消除单向发送
        for (int i = 0; i < list.size(); i++) {

            for (int j = i + 1; j < list.size(); ) {

                if (j == list.size()) {
                    break;
                }


                if (list.get(i).getSenderid().equals(list.get(j).getSenderid())&&list.get(i).getReceiverid().equals(list.get(j).getReceiverid())) {

                    list.remove(j);
                    break;
                } else {
                    j++;
                }

            }
        }


//        消除互相发送
        for (int i = 0; i < list.size(); i++) {

            for (int j = i + 1; j < list.size(); ) {

                if (j == list.size()) {
                    break;
                }


                if (list.get(i).getReceiverid().equals(list.get(j).getSenderid())&&list.get(i).getSenderid().equals(list.get(j).getReceiverid())) {

                    list.remove(j);
                    break;
                } else {
                    j++;
                }

            }
        }



        return list;

    }

    @Override
    public String getSendnameById(long id) {
        return userMapper.getUsernameById(id);
    }

    @Override
    public long getIdByUsername(String username) {

        return userMapper.getIdByUsername(username);
    }

    @Override
    public List<Message_User> getReturnMessage(long senderid, long receiverid) {
        return messageMapper.getReturnMessage(senderid, receiverid);
    }

    @Override
    public void sendMessage(Message message) {
        messageMapper.insertSelective(message);
    }

    @Override
    public void deleteMessage(Integer id) {
        messageMapper.deleteByPrimaryKey(id);
    }

    @Override
    public String getDpByid(long id) {

        return userMapper.getDpById(id);
    }


}
