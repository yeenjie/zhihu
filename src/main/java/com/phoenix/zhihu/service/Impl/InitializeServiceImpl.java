package com.phoenix.zhihu.service.Impl;

import com.phoenix.zhihu.entity.*;
import com.phoenix.zhihu.mapper.AnswerMapper;
import com.phoenix.zhihu.mapper.ArcticMapper;
import com.phoenix.zhihu.mapper.QuestionMapper;
import com.phoenix.zhihu.service.InitializeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Service
public class InitializeServiceImpl implements InitializeService {
    @Autowired
    QuestionMapper questionMapper;

    @Autowired
    AnswerMapper answerMapper;

    @Autowired
    ArcticMapper arcticMapper;

    /**
     * 获取热榜数据
     * */
    @Override
    public List<Question> getQuestions(){
        List<Question> list;
//        list = questionMapper.selectAll();
//        list = questionMapper.getHotData();
        QuestionExample example = new    QuestionExample();
        QuestionExample.Criteria criteria = example.createCriteria();

        //获取一个月前的日期
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Calendar c = Calendar.getInstance();
        Date today = new Date();
        c.setTime(today);
        c.add(Calendar.MONTH, -1);
        Date mbefore = c.getTime();
        String mon = format.format(mbefore);
        System.out.println("过去一个月："+mon);


        criteria.andCreatetimeBetween(mbefore,today);
        example.setOrderByClause("skimtimes desc");
        list = questionMapper.selectByExample(example);
        return list;
    }

    /**
     * 推荐界面数据
     * */
    @Override
    public List<AnswerList> getAnswerList() {
        List<AnswerList> list;
//        获取50条不重复的回答
        list = answerMapper.getDiffAnswers();

        return list;
    }

    /**
     *获取该问题的所有回答
     * */
    @Override
    public List<QuestionList> getQuestionList(int questionid) {
        List<QuestionList> list;
        list = questionMapper.getQuestionList(questionid);

        return list;
    }

    /**
     *获取某个问题的详情及其所有回答
     * */
    @Override
    public List<AnswerList> getAnswers(int questionid) {
        List<AnswerList> list;
//        获取某个问题的所有回答
        list = answerMapper.getAnswers(questionid);
        return list;

    }

    @Override
    public String getQuestionNameById(int questionid) {
        String questionname;
        questionname = questionMapper.getQuestionNameById(questionid);
        return questionname;
    }

    @Override
    public String getQuestionDescribeById(int questionid) {
        String questiondescribe;
        questiondescribe = questionMapper.getQuestionDescribeById(questionid);
        return questiondescribe;
    }

    @Override
    public String getQuestionSkimTimes(int questionid) {
        String questionskimtimes;
        questionskimtimes = questionMapper.getQuestionSkimTimes(questionid);
        return questionskimtimes;
    }

    @Override
    public void increaseSkimTimes(int questionid) {
        questionMapper.increaseSkimTimes(questionid);
    }

    @Override
    public List<Question> getAllQuestion() {
        List<Question> list = questionMapper.selectAll();
        return list;
    }

    @Override
    public List<ArticleList> getArticles() {
        List<ArticleList> list;
        list = arcticMapper.getArticles();
        return list;
    }
}
