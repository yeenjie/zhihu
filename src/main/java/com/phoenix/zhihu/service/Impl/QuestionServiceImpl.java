package com.phoenix.zhihu.service.Impl;

import com.phoenix.zhihu.entity.Question;
import com.phoenix.zhihu.entity.QuestionList;
import com.phoenix.zhihu.mapper.QuestionMapper;
import com.phoenix.zhihu.service.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class QuestionServiceImpl implements QuestionService {
    @Autowired
    QuestionMapper questionMapper;
    @Override
    public void addQuestion(Question question) {
        questionMapper.insert(question);
    }

    @Override
    public void deleteQuestionById(int id) {
        questionMapper.deleteByPrimaryKey(id);
    }

    @Override
    public void updateQuestion(Question question) {
        questionMapper.updateByPrimaryKey(question);
    }

    @Override
    public List<QuestionList> queryQuestion(String questionname) {
        return questionMapper.queryQuestion(questionname);
    }

    @Override
    public List<QuestionList> getQuestionByUserId(int userid) {
        return questionMapper.getQuestionByUserId(userid);
    }
}
