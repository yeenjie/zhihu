package com.phoenix.zhihu.service.Impl;

import com.phoenix.zhihu.entity.*;
import com.phoenix.zhihu.entity.resultObject.User_Collection;
import com.phoenix.zhihu.entity.resultObject.User_Follow;
import com.phoenix.zhihu.mapper.*;
import com.phoenix.zhihu.service.MyPageService;
import org.apache.ibatis.annotations.Select;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class MyPageServiceImpl implements MyPageService {

    @Autowired
    UserMapper userMapper;
    @Autowired
    FollowMapper followMapper;
    @Autowired
    CollectionnewMapper collectionnewMapper;
    @Autowired
    AnswerMapper answerMapper;
    @Autowired
    QuestionMapper questionMapper;

    @Override
    public List<User_Follow> getFollows(long uid) {
List<User_Follow> list1 = new ArrayList<>();
    List<Follow> list =  followMapper.getFollowId(uid);


    for(int i =0;i<list.size();i++){
        User_Follow user_follow = new User_Follow();
        user_follow.setFid(list.get(i).getFid());
        list1.add(user_follow);
        long longToInteger =  list1.get(i).getFid();
        Integer longToInteger1 = new  Long(longToInteger).intValue();
        User user = userMapper.selectByPrimaryKey(longToInteger1);
        list1.get(i).setFollowing(user.getFollowing());
        list1.get(i).setFollows(user.getFollows());
        list1.get(i).setUsername(user.getUsername());
        list1.get(i).setDp(user.getDp());
        list1.get(i).setResume(user.getResume());
        list1.get(i).setShow("true");

    }

        return list1;
    }

    @Override
    public void follow(long uid, long fid) {
        followMapper.follow(uid, fid);
    }

    @Override
    public void removeFollow(long uid, long fid) {
        followMapper.removeFollow(uid, fid);
    }

    @Override
    public List<User_Collection> getCollection(long id) {

        List<Collectionnew> list = collectionnewMapper.getCollection(id);
        List<User_Collection> list1  = new ArrayList<>();
        for(int i=0;i<list.size();i++){
            User_Collection user_collection = answerMapper.getById(list.get(i).getAnswerid());
            user_collection.setUserid(list.get(i).getId());
            user_collection.setQuestionname(questionMapper.getQuestionnameById(user_collection.getQuestionid()));

//            SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd-HH:mm:ss");
//            String s = sdf.format(user_collection.getAnswertime());
//            Date date =  sdf.parse(s);
//
//            user_collection.setAnswertime(date);
            user_collection.setAnswerid(list.get(i).getAnswerid());
            user_collection.setShow1("true");
            user_collection.setShow2("true");
            user_collection.setShow3("true");
            user_collection.setShow4("true");
            user_collection.setDp(userMapper.getDpById(list.get(i).getUserid()));
            list1.add(user_collection);
        }

        return list1;
    }


    @Override
    public void addAprove(int kind,long answerid) {
        if(kind == 1){
            answerMapper.addAprove(answerid);
        }
        else{
        answerMapper.removeAprove(answerid);
        }
    }

    @Override
    public void removeCollection(int kind, long uid, long answerid) {
        if(kind ==1){
            collectionnewMapper.insertCollection(uid,answerid);
        }
        else{
            collectionnewMapper.removeCollection(uid,answerid);
        }
    }

    @Override
    public List<AnswerList> getMyAnswer(long userid) {
        List<AnswerList> list = answerMapper.getMyAnswers(userid);
        return list;
    }


}
