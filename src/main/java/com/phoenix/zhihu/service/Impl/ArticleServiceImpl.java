package com.phoenix.zhihu.service.Impl;


import com.phoenix.zhihu.entity.Arctic;
import com.phoenix.zhihu.entity.ArticleDetail;
import com.phoenix.zhihu.entity.ArticleList;
import com.phoenix.zhihu.mapper.ArcticMapper;
import com.phoenix.zhihu.service.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ArticleServiceImpl implements ArticleService {
    @Autowired
    ArcticMapper arcticMapper;

    @Override
    public void addArticle(Arctic arctic) {
        arcticMapper.insert(arctic);
    }

    @Override
    public void addAproveArticle(String kind, String articleid) {
        if("1".equals(kind)){
            System.out.println("zanannsa");
            arcticMapper.addAprove(articleid);
        }else{
            arcticMapper.cancleAprove(articleid);
        }

    }

    @Override
    public ArticleList getArticleDetails(int articleid) {
        ArticleList articleList = arcticMapper.getArticleDetails(articleid);
        return articleList;
    }
}

