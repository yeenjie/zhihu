package com.phoenix.zhihu.service.Impl;

import com.phoenix.zhihu.entity.Answer;
import com.phoenix.zhihu.entity.AnswerList;
import com.phoenix.zhihu.mapper.AnswerMapper;
import com.phoenix.zhihu.mapper.AnswercommentMapper;
import com.phoenix.zhihu.service.AnswerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AnswerServiceImpl implements AnswerService {

    @Autowired
    AnswerMapper answerMapper;

    /**
     * 获取50条不同问题的回答
     * */
    @Override
    public List<AnswerList> getDiffAnswers() {
        List<AnswerList> list;
        list = answerMapper.getDiffAnswers();
        return list;
    }

    @Override
    public void increaseCommentTimes(int answerid) {
        answerMapper.increaseCommentTimes(answerid);
    }

    @Override
    public void addAnswer(Answer answer) {
        answerMapper.insert(answer);
    }
}
