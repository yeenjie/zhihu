package com.phoenix.zhihu.service.Impl;

import com.phoenix.zhihu.entity.AnswerList;
import com.phoenix.zhihu.entity.Answercomment;
import com.phoenix.zhihu.entity.CommentList;
import com.phoenix.zhihu.entity.ReplyList;
import com.phoenix.zhihu.mapper.AnswercommentMapper;
import com.phoenix.zhihu.service.AnswerCommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class AnswerCommentServiceImpl implements AnswerCommentService {
    @Autowired
    AnswercommentMapper answercommentMapper;

    @Override
    public List<CommentList> getAllComments(int answerid) {

        List<CommentList> list = answercommentMapper.selectAllComments(answerid);
        return list;
    }

    @Override
    public List<ReplyList> getAllReplies(int answerid, int bearticleid) {
        Map<String,Object> map = new HashMap();
        map.put("answerid",answerid);
        map.put("bearticleid",bearticleid);
        List<ReplyList> list = answercommentMapper.getAllReplies(map);
        return list;
    }

    @Override
    public void setComment(String content, int criticsid, int answerid, int beatricleid) {
        //获取当前时间
        Date utilDate = new Date();
        System.out.println("utilDate : " + utilDate);
        //util.Date转sql.Date
        java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());
        System.out.println("sqlDate : " + sqlDate);


        Answercomment answercomment = new Answercomment();
        answercomment.setAnswerid(answerid);
        answercomment.setBearticleid(beatricleid);
        answercomment.setContent(content);
        answercomment.setCriticsid(criticsid);
        answercomment.setCommenttime(sqlDate);
        answercommentMapper.insert(answercomment);
    }

    @Override
    public void setReply(String content, int criticsid, int becriticsid, int answerid, int beatricleid) {
        //获取当前时间
        Date utilDate = new Date();
        System.out.println("utilDate : " + utilDate);
        //util.Date转sql.Date
        java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());
        System.out.println("sqlDate : " + sqlDate);


        Answercomment answercomment = new Answercomment();
        answercomment.setAnswerid(answerid);
        answercomment.setBearticleid(beatricleid);
        answercomment.setBecriticsid(becriticsid);
        answercomment.setContent(content);
        answercomment.setCriticsid(criticsid);
        answercomment.setCommenttime(sqlDate);
        answercommentMapper.insert(answercomment);
    }
}
