package com.phoenix.zhihu.service.Impl;

import com.phoenix.zhihu.entity.User;
import com.phoenix.zhihu.mapper.UserMapper;
import com.phoenix.zhihu.service.UserService;
import com.phoenix.zhihu.utils.CookieUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserMapper userMapper;
    @Override
    public void addUserImage(int userid, String url) {
        User user = userMapper.selectByPrimaryKey(userid);
        user.setDp(url);
        userMapper.updateByPrimaryKey(user);
    }

    @Override
    public int getNowUserId(HttpServletRequest request) {
        String userid = CookieUtil.getValue(request.getCookies(),"zhihuid");
        return Integer.parseInt(userid);
    }


}
