package com.phoenix.zhihu.service.Impl;

import com.phoenix.zhihu.entity.User;
import com.phoenix.zhihu.mapper.UserMapper;
import com.phoenix.zhihu.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class LoginServiceImpl implements LoginService {

    @Autowired
    UserMapper userMapper;

    @Override
    public String Login(User user) {
       String password =  userMapper.getPasswordByName(user.getUsername());
       try{
           if(password.equals(user.getPassword())){
               return "登录成功";
           }
           else{
               return "登录失败";
           }
       }catch (Exception e){
           return "用户不存在";
       }

    }

    @Override
    public boolean register(User user) {
        try {
            userMapper.insertRegister(user.getUsername(), user.getPassword());
            System.out.println(user.getPassword());
            return true;
        } catch (Exception e) {
            return false;
        }


    }

    @Override
    public long getIdByUsername(String username) {
        return userMapper.getIdByUsername(username);
    }

    @Override
    public Boolean checkUsername(String username) {

        try{
            if(userMapper.checkUsername(username).getUsername().equals(username)){
                return true;
            }
            else return false;

        }catch (Exception e){
            return false;
        }


    }

    @Override
    public String getUsernameById(long id) {
        return userMapper.getUsernameById(id);
    }

}
