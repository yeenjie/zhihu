package com.phoenix.zhihu.service.Impl;

import com.phoenix.zhihu.entity.Collectionanswer;
import com.phoenix.zhihu.mapper.CollectionanswerMapper;
import com.phoenix.zhihu.service.CollectionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CollectionServiceImpl implements CollectionService{

    @Autowired
    CollectionanswerMapper collectionanswerMapper;

    @Override
    public List<Collectionanswer> getCollectionListByUserId(int userid) {
        return collectionanswerMapper.getCollectionListByUserId(userid);
    }
}
