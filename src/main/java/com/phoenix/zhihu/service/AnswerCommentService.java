package com.phoenix.zhihu.service;

import com.phoenix.zhihu.entity.CommentList;
import com.phoenix.zhihu.entity.ReplyList;
import org.springframework.stereotype.Service;

import java.util.List;


public interface AnswerCommentService {
    /**
     * 获取评论内容
     * */
    List<CommentList> getAllComments(int answerId);

    List<ReplyList> getAllReplies(int answerid, int bearticleid);

    void setComment(String content, int criticsid, int answerid, int beatricleid);

    void setReply(String content, int criticsid, int becriticsid, int answerid, int beatricleid);
}
