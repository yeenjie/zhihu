package com.phoenix.zhihu.service;

import com.phoenix.zhihu.entity.Answer;
import com.phoenix.zhihu.entity.AnswerList;
import org.springframework.stereotype.Service;

import java.util.List;


public interface AnswerService {

    List<AnswerList> getDiffAnswers();

    void increaseCommentTimes(int answerid);

    void addAnswer(Answer answer);

}
