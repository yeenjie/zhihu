package com.phoenix.zhihu.service;

import com.phoenix.zhihu.entity.AnswerList;
import com.phoenix.zhihu.entity.resultObject.User_Collection;
import com.phoenix.zhihu.entity.resultObject.User_Follow;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

public interface MyPageService {

    public List<User_Follow> getFollows(long id);
    public void follow(long uid,long fid);
    public void removeFollow(long uid,long fid);
    public List<User_Collection> getCollection(long id);
    public void addAprove(int kind,long answerid);
    public void removeCollection(int kind,long uid,long answerid);

    List<AnswerList> getMyAnswer(long userid);
}
