package com.phoenix.zhihu.service;

import com.phoenix.zhihu.entity.*;
import org.springframework.stereotype.Service;

import java.util.List;


public interface InitializeService {
    List<Question> getQuestions();

    List<AnswerList> getAnswerList();


    List<QuestionList> getQuestionList(int questionid);

    List<AnswerList> getAnswers(int questionid);

    String getQuestionNameById(int questionid);

    String getQuestionDescribeById(int questionid);

    String getQuestionSkimTimes(int questionid);

    void increaseSkimTimes(int questionid);

    List<Question> getAllQuestion();

    List<ArticleList> getArticles();
}
