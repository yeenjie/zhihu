package com.phoenix.zhihu.service;

import com.phoenix.zhihu.entity.Message;
import com.phoenix.zhihu.entity.resultObject.Message_User;

import java.util.List;
import java.util.Map;

public interface MessageService {

    public List<Message_User> getMessage(long receiverid);
    public List<Message_User> getNewMessage(long id);
    public String getSendnameById(long id);
    public long getIdByUsername(String username);
    public List<Message_User> getReturnMessage(long senderid,long receiverid);
    public void sendMessage(Message message);
    public void deleteMessage(Integer id);
    public String getDpByid(long id);
}
