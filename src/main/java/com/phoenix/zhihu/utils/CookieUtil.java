package com.phoenix.zhihu.utils;

import javax.servlet.http.Cookie;

public class CookieUtil {
    public static String getValue(Cookie[] cookies,String name){
        if (cookies==null) return null;
        for (Cookie cookie : cookies) {
            if(cookie.getName().equals(name))
                return cookie.getValue();
        }
        return null;
    }
}
