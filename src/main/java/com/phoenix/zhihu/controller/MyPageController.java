package com.phoenix.zhihu.controller;

import com.phoenix.zhihu.entity.AnswerList;
import com.phoenix.zhihu.entity.resultObject.User_Collection;
import com.phoenix.zhihu.service.MyPageService;
import com.phoenix.zhihu.utils.ResponseWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class MyPageController {

    @Autowired
    MyPageService myPageService;
    @RequestMapping("getFollows")
    public ResponseWrapper getFollows(@RequestParam("id") long id){
        return ResponseWrapper.markSuccess(myPageService.getFollows(id));
    }

    @RequestMapping("follow")
    public ResponseWrapper follow(@RequestParam("uid") long uid,@RequestParam("fid") long fid){
        myPageService.follow(uid, fid);
        return ResponseWrapper.markSuccessButNoData();
    }
    @RequestMapping("removeFollow")
    public ResponseWrapper removeFollow(@RequestParam("uid") long uid,@RequestParam("fid") long fid){
        myPageService.removeFollow(uid, fid);
        return ResponseWrapper.markSuccessButNoData();
    }

    @RequestMapping("getCollection")
    public ResponseWrapper getCollection(@RequestParam("id") long id)
    {

        return ResponseWrapper.markSuccess(myPageService.getCollection(id));
    }

    @RequestMapping("getDynamic")
    public ResponseWrapper getDynamic(@RequestParam("id") long id)
    {
        List<User_Collection> list =myPageService.getCollection(id);
        List<User_Collection> list1 =new ArrayList<>();

        for(int i=list.size()-1;i>=0;i--){
            list1.add(list.get(i));
        }

        return ResponseWrapper.markSuccess(list1);
    }
    @RequestMapping("addAprove")
    public ResponseWrapper addAprove(@RequestParam("kind") int kind,@RequestParam("answerid") long answerid){
        myPageService.addAprove(kind,answerid);
        return ResponseWrapper.markSuccessButNoData();
    }
    @RequestMapping("removeCollection")
    public ResponseWrapper removeCollection(@RequestParam("kind") int kind,@RequestParam("uid") long uid,@RequestParam("answerid") long answerid){
        myPageService.removeCollection(kind, uid, answerid);
        return ResponseWrapper.markSuccessButNoData();
    }

    @RequestMapping("myAnswer")
    public ResponseWrapper myAnswer(@RequestParam("userid") long userid){
        List<AnswerList> list = myPageService.getMyAnswer(userid);
        return ResponseWrapper.markSuccess(list);
    }

}
