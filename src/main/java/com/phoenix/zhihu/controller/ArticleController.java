package com.phoenix.zhihu.controller;

import com.phoenix.zhihu.entity.Arctic;
import com.phoenix.zhihu.entity.ArticleDetail;
import com.phoenix.zhihu.entity.ArticleList;
import com.phoenix.zhihu.service.ArticleService;
import com.phoenix.zhihu.service.UserService;
import com.phoenix.zhihu.utils.ResponseWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;

@Controller
public class ArticleController {
    @Autowired
    UserService userService;
    @Autowired
    ArticleService articleService;
    @RequestMapping("/upload_article")
    public String upload_article_image(String fileurl, String articlename, String articletext, HttpServletResponse response, HttpServletRequest request, Model model) throws Exception {
        System.out.println(fileurl);
        Cookie[] cookies = request.getCookies();
        int userid = userService.getNowUserId(request);
        Arctic arctic = new Arctic();
        arctic.setArticlename(articlename);
        arctic.setAuthorid(userid);
        arctic.setArticletext(articletext);
        arctic.setArticlepicture(fileurl);
        arctic.setCreatetime(new Date());
        arctic.setAgree(0);
        articleService.addArticle(arctic);
        return  "redirect:/";
    }

    @RequestMapping("/addAproveArticle")
    @ResponseBody
    public ResponseWrapper addAproveArticle(String kind, String articleid){
        System.out.println("kind++"+kind);
        System.out.println("answerid++"+articleid);
        articleService.addAproveArticle(kind,articleid);
        return ResponseWrapper.markSuccessButNoData();
    }

    @RequestMapping("/articleDetails")
    public String toArticleDetails(int articleid,Model m){
        ArticleList articleDetails = articleService.getArticleDetails(articleid);
        m.addAttribute("articleDetails",articleDetails);
        return "articleDetail";
    }
}
