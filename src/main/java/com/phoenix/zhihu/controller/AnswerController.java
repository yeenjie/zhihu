package com.phoenix.zhihu.controller;

import com.phoenix.zhihu.entity.Answer;
import com.phoenix.zhihu.service.AnswerService;
import com.phoenix.zhihu.utils.CookieUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.util.Date;

@Controller
public class AnswerController {
    @Autowired
    AnswerService answerService;

    @RequestMapping("/addAnswer")
    public String add(String questionid,String content, HttpServletRequest request){
        Cookie[] cookies = request.getCookies();
        int userid = Integer.parseInt(CookieUtil.getValue(cookies,"zhihuid"));
        Answer answer = new Answer();
        answer.setQuestionid(Integer.parseInt(questionid));
        answer.setContent(content);
        answer.setUserid(userid);
        answer.setCommenttimes(0);
        answer.setAnswertime(new Date());
        int questionId = answer.getQuestionid();
        answerService.addAnswer(answer);
        String reUrl = "redirect:/questionPage?questionid="+questionId;
        return  reUrl;
    }
}
