//package com.phoenix.zhihu.controller;
//
//import com.phoenix.zhihu.entity.User;
//import com.phoenix.zhihu.service.UserService;
//import com.phoenix.zhihu.utils.*;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.*;
//import org.springframework.web.multipart.MultipartFile;
//
//import javax.servlet.http.Cookie;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.io.File;
//import java.io.FileOutputStream;
//
///**
// * 服务器上传
// */
//@Controller
//public class FileUploadController {
//    @Autowired
//    UserService userService;
//    @RequestMapping("/upload")
//    @ResponseBody
//    public ImgInfo setImgUrl(MultipartFile file, HttpServletResponse response, HttpServletRequest request)
//            throws Exception {
//        // Get the file and save it somewhere
//        byte[] bytes = file.getBytes();
////        System.out.println(new String(bytes));
//        String path = request.getServletContext().getRealPath("/image/");
//        File imgFile = new File(path);
//        if (!imgFile.exists()) {
//            imgFile.mkdirs();
//        }
//        String fileName = file.getOriginalFilename();// 文件名称
//        System.out.println(path + fileName);
//
//        try (FileOutputStream fos = new FileOutputStream(new File(path + fileName))) {
//            int len = 0;
//            fos.write(bytes);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        String value = "http://localhost:8080/image/" + fileName;
//        String[] values = { value };
//
//        ImgInfo imgInfo = new ImgInfo();
//        imgInfo.setError(0);
//        imgInfo.setUrl(values);
//
//        System.out.println(imgInfo.toString());
//        return imgInfo;
//    }
//
//    @RequestMapping("/upload_dp")
//    @ResponseBody
//    public ImgInfo upload_dp(MultipartFile file, HttpServletResponse response, HttpServletRequest request) throws Exception {
//        ImgInfo imgInfo = setImgUrl(file,response,request);
//        Cookie[] cookies = request.getCookies();
//        int userid = Integer.parseInt(CookieUtil.getValue(cookies,"zhihuid"));
//        userService.addUserImage(userid,imgInfo.getUrl()[0]);
//        return  imgInfo;
//    }
//
//
//}