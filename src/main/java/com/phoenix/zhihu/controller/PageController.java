package com.phoenix.zhihu.controller;

import com.phoenix.zhihu.entity.AnswerList;
import com.phoenix.zhihu.entity.ArticleList;
import com.phoenix.zhihu.entity.Question;
import com.phoenix.zhihu.service.InitializeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
public class PageController {
    @Autowired
    InitializeService initializeService;
    @RequestMapping("/")
    public String toReco(Model m){
        List<AnswerList> list;

        //获取所有不重复的回答显示在推荐
        list = initializeService.getAnswerList();
        m.addAttribute("answerList",list);
        System.out.println(list);
        return "index";
    }

    @RequestMapping("/hotPage")
    public String toHot(Model m){
        List<Question> list;
        //获取50条的问题显示在热榜
        list = initializeService.getQuestions();
        m.addAttribute("questionList",list);
        return "indexHot";
    }

    @RequestMapping("/carePage")
    public String toCare(Model m){
        m.addAttribute("name","123");
        m.addAttribute("content","这是一个content");
        m.addAttribute("dateTime","这是一个datetime");
        return "indexCare";
    }

    @RequestMapping("/message")
    public String toMsg(){
        return "message";
    }

    @RequestMapping("/lookmessage")
    public String toLMsg(int receiverid, int senderid, ModelMap map){
        map.addAttribute("receiverid",receiverid);
        map.addAttribute("senderid",senderid);
        return "lookmessage";

    }

    @RequestMapping("/myself")
    public ModelAndView toMyself(){
        ModelAndView modelAndView = new ModelAndView("myself.html");
        return modelAndView;
    }

    @RequestMapping("/myselfFollow")
    public ModelAndView myselfFollow(){
        ModelAndView modelAndView = new ModelAndView("myselfFollow.html");
        return modelAndView;
    }

    @RequestMapping("/myselfCollect")
    public ModelAndView myselfCollect(){
        ModelAndView modelAndView = new ModelAndView("myselfCollection.html");
        return modelAndView;
    }


    /**
     * 已知：问题的id
     * 1.问题的标题
     * 2.问题的内容
     * 3.回答人的id
     * 4.回答的内容  th:utext
     * */
    @RequestMapping("/questionPage")
    public String toQues(int questionid,Model m){

        //进入问题页面对该问题的浏览次数+1
        initializeService.increaseSkimTimes(questionid);

        List<AnswerList> list = initializeService.getAnswers(questionid);
        m.addAttribute("qAnswerList",list);

        String questionname = initializeService.getQuestionNameById(questionid);
        String questiondescribe = initializeService.getQuestionDescribeById(questionid);
        String skimtimes = initializeService.getQuestionSkimTimes(questionid);
        System.out.println("skimtimes+++"+skimtimes);
        m.addAttribute("questionname",questionname);
        m.addAttribute("questiondescribe",questiondescribe);
        m.addAttribute("skimtimes",skimtimes);
        return "question";
    }

    @RequestMapping("/write")
    public ModelAndView write(){
        ModelAndView modelAndView = new ModelAndView("write.html");
        return modelAndView;
    }


    @RequestMapping("/waitAnswerPage")
    public String toWaitAnswer(Model m){
        List<Question> list;
        list = initializeService.getAllQuestion();
        m.addAttribute("questionList",list);
        return "waitYourAnswer";
    }

    @RequestMapping("/discoveryPage")
    public String toDiscovery(Model m) {
        List<ArticleList> list = initializeService.getArticles();
        m.addAttribute("articleList",list);
        return "indexDiscovery";
    }
}
