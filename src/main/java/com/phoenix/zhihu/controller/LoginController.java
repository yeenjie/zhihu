package com.phoenix.zhihu.controller;


import com.phoenix.zhihu.entity.User;
import com.phoenix.zhihu.service.LoginService;
import com.phoenix.zhihu.utils.ResponseWrapper;
import com.phoenix.zhihu.utils.ReturnCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LoginController {

    @Autowired
    LoginService loginService;

    @RequestMapping("/login")
    public ResponseWrapper loginCheck(User user) {

        System.out.println(user.getPassword());
        if (loginService.Login(user).equals("登录成功")) {
            return ResponseWrapper.markSuccessButNoData();

        } else if (loginService.Login(user).equals("登录失败")) {
            return ResponseWrapper.markCustom(false, ReturnCode.FEAILED.getCode(), ReturnCode.FEAILED.getMsg(), loginService.Login(user));

        } else {
            return ResponseWrapper.markCustom(false, ReturnCode.ACCOUNT_ERROR.getCode(), ReturnCode.ACCOUNT_ERROR.getMsg(), loginService.Login(user));
        }
    }

    /**
     * 登录成功无返回值
     * 登录失败有失败字符
     */
    @RequestMapping("/register")
    public ResponseWrapper register(User user) {
        if (!loginService.Login(user).equals("用户不存在")) {
            return ResponseWrapper.markCustom(false, ReturnCode.FEAILED.getCode(), ReturnCode.FEAILED.getMsg(), "用户存在");
        } else {
            if (loginService.register(user)) {
                return ResponseWrapper.markSuccessButNoData();
            } else {
                return ResponseWrapper.markCustom(false, ReturnCode.SYSTEM_ERROR.getCode(), ReturnCode.SYSTEM_ERROR.getMsg(), "系统异常");
            }
        }
    }

    @RequestMapping("/getIdByUsername")
    public long getIdByUsername(String username) {
        return loginService.getIdByUsername(username);
    }

    @RequestMapping("/getUsernameById")
    public ResponseWrapper getUsernameById(long id){

        return ResponseWrapper.markSuccess(loginService.getUsernameById(id));
    }

    @RequestMapping("checkUsername")
    public  ResponseWrapper checkUsername(String username ){

        if(loginService.checkUsername(username)){
            return ResponseWrapper.markSuccess("true");
        }
        return ResponseWrapper.markError();
    }
}
