package com.phoenix.zhihu.controller;

import com.auth0.jwt.internal.org.bouncycastle.math.raw.Mod;
import com.phoenix.zhihu.entity.Question;
import com.phoenix.zhihu.entity.QuestionList;
import com.phoenix.zhihu.service.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.List;

@Controller
public class QuestionController {

    @Autowired
    QuestionService questionService;

    @RequestMapping("/addQuestion")
//    @ResponseBody
    public String addQuestion(Question question) {

        //question.setUserid(userid);
        question.setAnswertimes(0);
        question.setSkimtimes(0);
        question.setCreatetime(new Date());
        System.out.println(question);
        questionService.addQuestion(question);
//        返回问题描述页面
//        ModelAndView modelAndView = new ModelAndView("/index");
//        return modelAndView;
        //应转发至该问题的详情页
        return "redirect:/";
    }

    @RequestMapping("/deleteQuestion")
    @ResponseBody
    public ModelAndView deleteQuestion(int id) {
        questionService.deleteQuestionById(id);
//        暂时不知道返回到哪
        ModelAndView modelAndView = new ModelAndView("index");
        return modelAndView;
    }

    @ResponseBody
    @RequestMapping("/updateQuestion")
    public ModelAndView updateQuest(Question question) {
        questionService.updateQuestion(question);
//        返回问题描述页面
        ModelAndView modelAndView = new ModelAndView("index");
        return modelAndView;
    }


    @RequestMapping("/queryQuestion")
    @ResponseBody
    public ModelAndView queryQuestion(String question) {
        ModelAndView modelAndView = new ModelAndView("indexSearchResult");
        question = "%" + question + "%";
        System.out.println(question);
        List<QuestionList> list;
        list = questionService.queryQuestion(question);
        System.out.println(list);
        modelAndView.addObject("searchList", list);
//        session.setAttribute("searchList", list);
        //ModelAndView modelAndView = new ModelAndView("/searchPage");
        //return modelAndView;
        return modelAndView;
    }

    @RequestMapping("/queryQuestionByUserId")
    @ResponseBody
    public ModelAndView queryQuestionByUserId(int userid) {
        ModelAndView modelAndView = new ModelAndView("myselfQuestion");
        System.out.println(userid);
        List<QuestionList> list;
        list = questionService.getQuestionByUserId(userid);
        System.out.println(list);
        modelAndView.addObject("questionList", list);
//        session.setAttribute("searchList", list);
        //ModelAndView modelAndView = new ModelAndView("/searchPage");
        //return modelAndView;
        return modelAndView;
    }
}
