package com.phoenix.zhihu.controller;

import com.phoenix.zhihu.entity.Collectionanswer;
import com.phoenix.zhihu.service.CollectionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@RestController
public class CollectionController {

    @Autowired
    CollectionService collectionService;

    @RequestMapping("/findCollections")
    public ModelAndView findCollections(int userid) {
        ModelAndView modelAndView = new ModelAndView("myselfCollection");
        System.out.println(userid);
        List<Collectionanswer> list;
        list = collectionService.getCollectionListByUserId(userid);
        System.out.println(list);
        modelAndView.addObject("collectionList", list);
        return modelAndView;
    }
}
