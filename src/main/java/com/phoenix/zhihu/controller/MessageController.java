package com.phoenix.zhihu.controller;


import com.phoenix.zhihu.entity.Message;
import com.phoenix.zhihu.entity.resultObject.Message_User;
import com.phoenix.zhihu.service.LoginService;
import com.phoenix.zhihu.service.MessageService;
import com.phoenix.zhihu.utils.ResponseWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@RestController
public class MessageController {

    @Autowired
    MessageService messageService;
    @Autowired
    LoginService loginService;

    @RequestMapping("/getMessage")

    public ResponseWrapper getMessage(@RequestParam("username") String username) {

        long id = messageService.getIdByUsername(username);
        List<Message_User> list = messageService.getMessage(id);
        if (list == null) {
            return ResponseWrapper.markError();
        } else {
            for (int i = 0; i < list.size(); i++) {
                list.get(i).setSendername(messageService.getSendnameById(list.get(i).getSenderid()));
                list.get(i).setReceivername(messageService.getSendnameById(list.get(i).getReceiverid()));
                list.get(i).setDp(messageService.getDpByid(list.get(i).getSenderid()));
            }
            return ResponseWrapper.markSuccess(list);
        }
    }

    @RequestMapping("getNewMessage")
    public ResponseWrapper getNewMessage(@RequestParam("username") String username) {
        long id = messageService.getIdByUsername(username);

        List<Message_User> list = messageService.getNewMessage(id);

        if (list != null) {
            for (int i = 0; i < list.size(); ) {
                ++i;
                --i;
                list.get(i).setSendername(messageService.getSendnameById(list.get(i).getSenderid()));
                list.get(i).setReceivername(messageService.getSendnameById(list.get(i).getReceiverid()));
                list.get(i).setDp(messageService.getDpByid(list.get(i).getSenderid()));
                i++;
            }
            return ResponseWrapper.markSuccess(list);


        } else {
            return ResponseWrapper.markError();
        }
    }


    @RequestMapping("getReturnMessage")
    public ResponseWrapper getReturnMessage(@RequestParam("senderid") int senderid, @RequestParam("receiverid") int receiverid) {

        List<Message_User> list = messageService.getReturnMessage(senderid, receiverid);
        for (int i = 0; i < list.size(); ) {
            list.get(i).setSendername(messageService.getSendnameById(list.get(i).getSenderid()));
            list.get(i).setReceivername(messageService.getSendnameById(list.get(i).getReceiverid()));

            list.get(i).setDp(messageService.getDpByid(list.get(i).getSenderid()));
            i++;
        }

        return ResponseWrapper.markSuccess(list);
    }



    @RequestMapping("sendMessage")
    public ResponseWrapper sendMessage(Message message) {
        java.util.Date date1 = new java.util.Date();   // 获取当前时间
        java.sql.Date sql_date = new java.sql.Date(date1.getTime()); //转换成java.sql.Date

        message.setCtreatetime(sql_date);

        messageService.sendMessage(message);
        return ResponseWrapper.markSuccessButNoData();
    }

    @RequestMapping("findSendMessage")
    public ResponseWrapper findSendMessage(Message message,@RequestParam("receivername") String receivername){
        long receiverid =  loginService.getIdByUsername(receivername);
        Integer receiverNewId = new  Long(receiverid).intValue();
        message.setReceiverid(receiverNewId);
        java.util.Date data1 = new java.util.Date();   // 获取当前时间
        message.setCtreatetime(data1);

        messageService.sendMessage(message);
        return ResponseWrapper.markSuccessButNoData();
    }


    @RequestMapping("deleteMessage")
    public ResponseWrapper deleteMessage(@RequestParam("id") Integer id) {
        messageService.deleteMessage(id);
        return ResponseWrapper.markSuccessButNoData();
    }

}
