package com.phoenix.zhihu.controller;

import com.phoenix.zhihu.entity.Answercomment;
import com.phoenix.zhihu.entity.CommentList;
import com.phoenix.zhihu.entity.ReplyList;
import com.phoenix.zhihu.mapper.AnswercommentMapper;
import com.phoenix.zhihu.service.AnswerCommentService;
import com.phoenix.zhihu.service.AnswerService;
import com.phoenix.zhihu.utils.ResponseWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 获取评论相关信息
 * */
@RestController
public class CommentController {

    @Autowired
    AnswerCommentService answerCommentService;

    @Autowired
    AnswerService answerService;
    /**
     *获取评论  评论者的id（），评论内容content，,
     * */
//    @RequestMapping("/getComments")
//    public List<CommentList> getAllComment(int answerid){
//        List<CommentList> list = answerCommentService.getAllComments(answerid);
//        //m.addAttribute("commentsList",list);
//        return list;
////    }

    @RequestMapping("/getComments")
    public ResponseWrapper getAllComment(int answerid, Model m){
        List<CommentList> list = answerCommentService.getAllComments(answerid);
        System.out.println("commentList");
        m.addAttribute("commentList",list);
        for (int index=0;index<list.size();index++) {
            System.out.println("content:"+list.get(index).getContent());
            System.out.println("bearticleid"+list.get(index).getBearticleid());
            System.out.println("criticsid"+list.get(index).getCriticsid());
            System.out.println("username"+list.get(index).getUsername());
            System.out.println("dp"+list.get(index).getDpPath());
        }
        return ResponseWrapper.markSuccess(list);
    }

    /**
     * 获取回复
     * */
    @RequestMapping("/getReplies")
    public List<ReplyList> getAllReplies(int answerid, int bearticleid){
        List<ReplyList> list = answerCommentService.getAllReplies(answerid,bearticleid);
        return list;
    }

    /**
     * 发表回复
     * */
    @RequestMapping("replying")
    public void setReply(String content, int criticsid, int becriticsid, int answerid, int beatricleid){
        answerCommentService.setReply(content,criticsid,becriticsid,answerid,beatricleid);
    }

    /**
     * 发表评论
     * */
    @RequestMapping("commenting")
    public void setComment(String content, int criticsid, int answerid, int bearticleid){
        answerCommentService.setComment(content,criticsid,answerid,bearticleid);
//        评论次数+1
        answerService.increaseCommentTimes(answerid);

    }
}