package com.phoenix.zhihu.controller;


import com.phoenix.zhihu.entity.User;
import com.phoenix.zhihu.entity.resultObject.Article_Admin;
import com.phoenix.zhihu.entity.resultObject.Question_Admin;
import com.phoenix.zhihu.entity.resultObject.User_Admin;
import com.phoenix.zhihu.mapper.AdminMapper;
import com.phoenix.zhihu.utils.ResponseWrapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class AdminController {

    @Autowired
    AdminMapper adminMapper;

    @RequestMapping("getUser")
    public ResponseWrapper getUser() {

        List<User_Admin> list = adminMapper.getUser();


        for (int i = 0; i < list.size(); i++) {
//            User_Admin user_admin = new User_Admin();
//            user_admin.setId(list.get(i).getId());
//            user_admin.setEmail(list.get(i).getEmail());
//            user_admin.setTelnum(list.get(i).getTelnum());
//            user_admin.setUsername(list.get(i).getUsername());
            if (list.get(i).getRole() != 0) {
                list.get(i).setShow1(true);
            } else {
                list.get(i).setShow1(false);
            }

        }


        return ResponseWrapper.markSuccess(list);
    }

    @RequestMapping("setUserStatus")
    public ResponseWrapper setUSerStatus(@RequestParam("kind") int kind, @RequestParam("userid") long userid) {
        if (kind == 1) {
            adminMapper.setUserStatus1(userid);
        } else {
            adminMapper.setUserStatus0(userid);
        }
        return ResponseWrapper.markSuccessButNoData();
    }

    @RequestMapping("getQuestion")
    public ResponseWrapper getQuestion() {
        List<Question_Admin> list = adminMapper.getQuestion();
        for (int i = 0; i < list.size(); i++) {
            list.get(i).setUsername(adminMapper.getUsernameById(list.get(i).getUserid()));
        }
        return ResponseWrapper.markSuccess(list);
    }

    @RequestMapping("getArticle")
    public ResponseWrapper getArticle() {
        List<Article_Admin> list = adminMapper.getArticle();

        for (int i = 0; i < list.size(); i++) {
            list.get(i).setAuthorname(adminMapper.getUsernameById(list.get(i).getAuthorid()));
        }
        return ResponseWrapper.markSuccess(list);
    }

    @RequestMapping("deleteQuestionByAdmin")
    public ResponseWrapper deleteQuestion(@RequestParam("id") long id) {
        adminMapper.deleteQuestion(id);
        return ResponseWrapper.markSuccessButNoData();
    }

}
