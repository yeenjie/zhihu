$(function () {
    $(".ContentItem-more").click(function () {
        var index = $(".ContentItem-more").index(this);
        //页面展开
        $(".RichContent").eq(index).removeClass("Highlight");
        $(".RichContent").eq(index).removeClass("is-expanded");
        $(".ContentItem-more").eq(index).css("display", "none");
        $(".ContentItem-rightButton").eq(index).css("display", "block");
        $(".CopyrightRichText-richText").eq(index).find("p").removeClass("content-hide");
        $(".CopyrightRichText-richText").eq(index).find("p:not(:first)").css("display", "block");
    });

    $(".ContentItem-rightButton").click(function () {
        var index = $(".ContentItem-rightButton").index(this);
        //页面收缩
        $(".RichContent").eq(index).addClass("Highlight");
        $(".RichContent").eq(index).addClass("is-expanded");
        $(".ContentItem-more").eq(index).css("display", "inline-block");
        $(".ContentItem-rightButton").eq(index).css("display", "none");
        $(".CopyrightRichText-richText").eq(index).find("p:not(:first)").css("display", "none");
        $(".CopyrightRichText-richText").eq(index).find("p:first").addClass("content-hide");
    });
});