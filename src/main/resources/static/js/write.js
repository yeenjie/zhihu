$(function(){
    // alert("test");
    var E = window.wangEditor;
		// var editor = new E('#editor-tools', '#editor-area')
		// 或者 var editor = new E( document.getElementById('editor') )
		var editor = new E('#editor-tools', '#editor-area');
		editor.customConfig.menus = [
			'head',
			'bold',
			'italic',
			'underline',
			'image',
			'video'
		]
		// 配置服务器端地址
		editor.customConfig.uploadImgServer = '/upload'
		// 将图片大小限制为 3M
		editor.customConfig.uploadImgMaxSize = 3 * 1024 * 1024;
		// 自定义上传参数
		editor.customConfig.uploadFileName = 'file';
		editor.customConfig.uploadImgHooks = {
			// 如果服务器端返回的不是 {errno:0, data: [...]} 这种格式，可使用该配置
			// （但是，服务器端返回的必须是一个 JSON 格式字符串！！！否则会报错）
			customInsert : function(insertImg, result, editor) {
				// 图片上传并返回结果，自定义插入图片的事件（而不是编辑器自动插入图片！！！）
				// insertImg 是插入图片的函数，editor 是编辑器对象，result 是服务器端返回的结果
				// 举例：假如上传图片成功后，服务器端返回的是 {url:'....'} 这种格式，即可这样插入图片：
				var url = result.url
				insertImg(url)
				// result 必须是一个 JSON 格式字符串！！！否则报错
			}
		}
		// 必须放到有关于编辑器设置前面
		editor.create();

	
      
        $("#fabu").click(function(){
            $("#articletext").attr("value",editor.txt.html());
            $("#article_form").submit();
        });
        var input = document.getElementById("file_input");
        // alert(input);
        input.addEventListener('change', readFile, false);
        function readFile() {
            var file = this.files[0]; //获取file对象
            var formData = new FormData($("#uploadForm")[0])  //创建一个forData 
            formData.append('file', file) //把file添加进去  name命名为img
            $.ajax({
                url: "/upload",
                data: formData,
                type: "POST",
                async: false,
                cache: false,
                contentType: false,
                processData: false,
                success: function (data) {
                    // alert(data.url);
                    $("#fileurl").attr("value",data.url);
                    var imgdisplay = "<img class='img-thumbnail' src='"+data.url +"' style='height:200px ;width:400px;'>"
                    $('#image-display').append(imgdisplay);
                },
                error: function () {
                    //失败
                }
            })
        }

    //

    

});
